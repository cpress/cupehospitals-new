+++
categories = ["Newsletters"]
date = "2018-09-06T17:45:08+00:00"
description = "Updates on the central contract implementation, news about the upcoming merger of Our Shared Purpose, and information about member outreach this Fall."
featured = "uploads/newsletter.jpg"
featuredpath = "img"
title = "1144-1590 Sept Newsletter"
type = "post"

+++
## Merger Update

The Ontario Labour Relations Board (OLRB) determined that there will be one fulltime & part-time Service bargaining unit and one fulltime & part-time Clerical bargaining unit for the newly merged hospital.

This means that there will be a vote of all Service employees at all 3 hospitals between CUPE and SEIU and a separate vote of all Clerical employees at all 3 hospitals between CUPE and non-union.

There is a dispute as to which bargaining unit the unit clerks/ward aids should be in.  Currently they are in the CUPE service units at both St. Joe’s and Providence.  At St. Mike’s they are non-union.  CUPE’s position is that they should stay in the service unit.  However, both SEIU and the employer are taking the position they should go in the clerical unit.  SEIU does not want the unit clerks/ward aids in the service unit because that increases CUPE’s numbers in the upcoming vote.

The OLRB has not yet set a Vote Date and it seems unlikely that a vote will take place before late Fall.

## Central Agreement Implementation

The Ontario Council of Hospital Unions (OCHU) filed an Implementation Dispute with the Ontario Hospital Association (OHA).  The Arbitration is set for September 24, 2018 before Arbitrator Mitchell.

The Hospital and the Unions have agreed to let the arbitration process proceed before dealing with the Unfair Labour Complaints at the OLRB on this issue.  Ideally, the arbitration Decision will resolve all the issues and we can focus on the merger and the upcoming vote.

Both the Hospital and SEIU had wanted to proceed with the merger vote before the implementation issue was resolved.  CUPE refused to do this because it put your retro pay and the upcoming wage increase in jeopardy.  If the vote were to take place before the central contract implementation issue was resolved, and by some chance CUPE wasn’t successful in both the Service and Clerical votes, then the employer would not have to pay the 1 year of retro pay to our members or the upcoming increase of 1.4% on September 29, 2018.

> SEIU at St. Mike’s is not part of the Central Bargaining Process and they currently do not have a contract and no upcoming wage increases.

## Know the Facts

Under the Public Sector Labour Relations Transition Act (PSLRTA) you DO NOT get the successful union’s collective agreement, wages or benefits except for the following 4 areas:

* Seniority
* Grievance Process
* Job Posting Language, and
* Lay off/Recall language including Job Security/Bumping

If you compare wages and benefits between all three hospital locations, there are differences at all three.  In some cases, a job rate for one job is higher at one hospital, but for another job it’s lower.  Its because of these differences CUPE has a Harmonization process.  After the vote if CUPE is the successful union, you will not only get the protection of CUPE’s Central Collective Agreement until 2021 which provides for additional wage increases, but you will also have CUPE negotiate the Local Issues Agreement.

It’s in the Local Issues bargaining that Harmonization takes place.  CUPE looks at all the collective agreements and identifies the ‘best of’ wages, benefits and working conditions and negotiates up to the highest.  CUPE has never lost an arbitration for harmonizing wages up to the highest.  In addition, any local issue that is being bargained by CUPE across the Province will also be negotiated and arbitrated including the wage adjustment for RPNs.

> All CUPE hospital locals are going to arbitration for an RPN rate of $33.51 which is 75% of the ONA RN 8 year rate.

Remember, SEIU at St. Mike’s isn’t part of the SEIU Central Bargaining process and they do not have a collective agreement.  They will have to negotiate every part of the collective agreement including wage increases.  With a Conservative Government in Ontario and cuts to funding this will be a difficult time to be in bargaining.

## CUPE Information Meetings

CUPE held several information meetings for members over the past month with great attendance.  Additional meetings will be scheduled for September and October.

#### Providence

* August 14, 15, 16, 22, 23
* September 4

#### St. Joe’s

* August 1, 28, 29
* September 5, 11 and 12.