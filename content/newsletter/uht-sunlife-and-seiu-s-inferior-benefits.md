+++
categories = ["News", "Newsletters"]
date = "2019-08-01T00:00:00-04:00"
description = "July 2019 Newsletter CUPE L1144 & 1590 UHT"
featured = "uploads/CUPENurse50yrs.jpg"
featuredpath = "img"
tags = ["UHT Service", " Unity Health Toronto"]
title = "UHT, SUNLIFE AND SEIU's Inferior Benefits"
type = "post"

+++
## ST MIKE’S & SEIU INFERIOR BENEFITS

Since UHT changed the benefit carrier at St Joe’s and Providence to Sun Life, the provider for St Mike’s and SEIU, CUPE members have reported that UHT is trying to force CUPE members to take the inferior benefits plan agreed to between SEIU and UHT.

CUPE has superior benefits to SEIU and these benefits are set out in the CUPE Central Collective Agreement.  Some of the changes UHT is trying to force on CUPE members are:

•	Charges for dispensing fees (CUPE members aren’t required to pay dispensing fees).

•	Cost for prescription drugs that previously there was no charge for.

•	Requirement to use ‘approved suppliers’ (CUPE does not have this requirement).

•	Required to accept generic brand of drugs (CUPE does not have this requirement).

•	Coverage for compression socks of only $160 (CUPE’s coverage is $200)

•	Scaling at dentist reduced down from 4 times/year (CUPE has 4 times/year)

•	Charges for Dinitrogen Monoxide (laughing gas) (No charge for CUPE members)

## BENEFIT  GRIEVANCES

Both CUPE Local 1590 and Local 1144 have filed Policy Grievances on the changes made to the benefit coverage.  In addition, individual grievances have been filed for those members who have experienced a cost due to these changes and contacted their local executive because they have.

If you incur a cost as a result of a change to the benefits coverage by UHT make sure to:

•	keep your receipt.

•	contact Sun Life directly about any claims that you had to pay that should have been  
covered to get these reimbursed.

•	Notify one of your local stewards or an executive member and provide them with a  
copy of the receipt and details of the change in coverage if Sun Life refuses to  
reimburse you for these costs.

Benefits is a Central Collective Agreement issue and as a result the Ontario Council of Hospital Unions will be taking the benefits grievance forward to arbitration.

OCHU/CUPE will protect the hard won benefits our members have and will not settle for inferior benefits negotiated between St Mike’s and SEIU.

## LABOUR DAY PARADE

![](/img/uploads/cupelabday.png)

Come out and join other CUPE members from across the GTA for a day of solidarity!  CUPE hats are being provided for all UHT CUPE members and of course you get free admission to the CNE.  If you plan to attend you need to, sign up [here](https://docs.google.com/forms/d/e/1FAIpQLSe9ibj3PNnjP_rxPydKAbvfx5mFkvIBC3elvbWKQQBc4eD_Ng/viewform "labour day") or speak to a local executive member.

## MERGER VOTE

There is a tentative agreement between UHT and the unions that the Service Vote should take place in September.  The Campaign will start September 4th and will run till either September 17th or 18th.

CUPE wants there to be 4 days of voting.  The first day would be electronic voting so that part-time, casual and people who are away from work can vote.  The next 3 days would be an on-site, paper vote by secret ballot.

**Both SEIU and the employer don’t want to have the one day of electronic voting.**

**Without electronic voting many part-time and casual employees will lose their ability to vote.**

**This doesn’t seem right, and no union should be trying to get in the way of having union members exercise their rights.**

## Stay Up To Date

Your union is hard at work making sure you are up-to-date with the latest information on changes in the workplace. The best way to stay up-to-date is to visit www.cupehospitals.ca and sign up to our mailing list.

## Nurse who has worked for 50 years honoured July 31 at Providence Healthcare

> ![](/img/uploads/CUPENurse50yrs.jpg)  
> _From left: OCHU Secretary- Treasurer Sharon Richer, Providence Chief Steward and RPN Lisa Ross; RPN Elisa Deocampo, OCHU President Michael Hurley and Providence VP Elkin Greg._

Elisa Deocampo, a Registered Practical Nurse who has worked for over 50 years at Providence Healthcare, received special recognition at the hospital on July 31. “Elisa is a wonderful example of dedication to patients and to her co-workers”, says Daniel Callaghan, the president of CUPE local 1590, which represents staff at the facility. “Ms. Deocampo shows great compassion and demonstrates considerable skill in her nursing. We are incredibly proud of her.”

Lisa Ross, also a Registered Practical Nurse at Providence Healthcare says “Very few people love their jobs enough to work at them for more than 50 years. What makes Elisa so special is that she blends a very high level of nursing skill and all of this experience with love for her patients. All of the nursing staff and patients adore her.”

“Think of all of the changes that have taken place in the healthcare system and in nursing care since 1966. One constant for patients at Providence Healthcare has been Elisa Deocampo, working day in and day out and year in and year out, who has done her absolute best every day to make life better for the people that she cares for. Elisa Deocampo loves her work and we are very grateful to her for her contribution to healthcare at Providence and in Ontario,” says Michael Hurley, president of the Ontario Council of Hospital Unions/CUPE.

## **TOWNHALL CALL**

CUPE/OCHU and L1590 and L1144 will be holding a Townhall Call on Wednesday August 14th at 7:00 p.m. You will receive a call a few days prior to this with the call information.