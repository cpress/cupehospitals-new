+++
categories = ["Newsletters"]
date = "2019-03-08T00:00:00-05:00"
description = "There is a lot of information on the merger and which union will best represent you after the vote.  To make an informed decision you need to be able to tell fact from misinformation. "
featured = "uploads/document-magnifier.jpg"
featuredpath = "img"
title = "Fact vs Fiction – How to tell the Difference"
type = "post"

+++
# Fact vs Fiction – How to tell the Difference

It has come to CUPE’s attention that SEIU representatives, in particular Richard Saladziak, has been providing misinformation to our members. Mr. Saladziak has _inaccurately_ informed many members that CUPE does not have retiree benefits for those that retire early. 

**This is of course not true.** 

Article 18.01e) of the CUPE Central Collective Agreement clearly states that the hospital will provide equivalent benefit coverage to early retirees to age 65:

> _18.01e)_
>
> _The Health Centre will provide equivalent coverage to all employees who retire early and have not yet reached age 65 and who are in receipt of the Health Centre's pension plan benefits on the same basis as is provided to active employees for semi-private, extended health care and dental benefits. The Health Centre will contribute the same portion towards the billed premiums of these benefits plans as is currently contributed by the Health Centre to the billed premiums of active employees._

There will be a lot of misinformation out there, some on purpose and some by people who are well meaning but just have the facts wrong. The best way to know whether someone is telling you the truth is to ask them to show it to you in writing. CUPE has early retiree benefits and SEIU does not so we are making sure we put it in writing in this update. All the information that CUPE provides is available in [Fact Sheets](http://cupehospitals.ca/facts/ "fact sheets") that our organizers are handing out. 

If SEIU won’t put it in writing there is a reason, it’s fake news!

### Who has your information?

Many of you have seen unknown people in the hospitals claiming to be from the ‘the union’ and asking for your personal contact information. You should be aware that your CUPE organizers and representatives will not simply say they are ‘from the union’. 

CUPE organizers will proudly say they are from CUPE and they will have the following badges which identify them as CUPE.

![](/img/uploads/small Ask Me Tab Side A.png)![](/img/uploads/small Ask Me Tag side B.jpg)

If someone approaches you claiming to be ‘from the union’ and they can’t identify themselves as CUPE organizers then you should call security.

Several times over the last few weeks SEIU representatives have accessed both St Joe’s and Providence and in some cases they have left flyers in food prep areas contaminating them and causing more work for our members when they arrive at work the next morning as they have to re-clean these areas.

Also, SEIU reps have been accessing the locked unit in the Houses of Providence placing vulnerable residents at risk.

While both unions are accessing all the workplaces our CUPE organizers will identify themselves as CUPE reps and they are familiar with working in a hospital and will not do anything that will put patients or residents at risk.

### Rally- NO to Cuts & Privatization!

CUPE is working with the Ontario Health Coalition and several other unions (OPSEU, ONA & Unifor).  All hospital & healthcare workers are encouraged to come out to the rally on April 30th.  Please speak with a CUPE organizer or CUPE Local Executive member for more information or watch our website for further information or email info@cupehospitals.ca.

![](/img/uploads/rally-ohc-2019-04-23-en-01_orig (2).png)