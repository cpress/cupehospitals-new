+++
categories = ["News"]
date = "2019-03-01T09:00:00-05:00"
description = "Town hall meetings to launch local fight-back campaigns/build capacity leading to April 30 rally."
featured = "uploads/march_news.png"
featuredpath = "img"
title = "Town hall meetings announced to fight against health care privatization"
type = "post"

+++
> [Download the Ontario Health Coalition's impact assessment.](https://www.ochu.on.ca/uploads/9/6/7/6/96767358/analysis_of_the_legislation.pdf)

Town hall meetings to launch local fight-back campaigns/build capacity leading to April 30 rally:

#### St. Catharines

* Monday March 11th
* 7:00 p.m.
* St. Catharines Kiwanis Aquatic Centre, Irene-Lawrence Room, 425 Carlton St.

#### Welland

* Tuesday March 12th
* 7:00 p.m.
* Welland City Hall

#### Hamilton

* Wednesday March 13th
* 7:00 p.m.
* Hamilton Central Library

#### Grimsby

* Thursday March 14th
* 7:00 p.m.
* Grimsby City Hall

#### Peterborough

* Wednesday March 20
* 6:00 p.m.
* Peterborough Public Library

#### Chatham

* Thursday March 21st
* 7:00 p.m.
* St. Clair College, 1001 Grand Avenue West (Red Line Room)

#### Windsor

* Friday March 22
* 6 p.m.
* Royal Canadian Legion Ambassador Br. 143, 1570 Marentette Ave.

#### Sarnia

* Saturday March 23
* 10:00 a.m.
* Royal Canadian Legion Br. 62, 286 Front St. N.

#### Stratford

* Saturday March 23
* 3:00 p.m.
* Royal Canadian Legion Br. 8, 804 Ontario St. B1.

#### London

* Monday, March 25th
* 7:00 p.m.
* London Public Library, 251 Dundas Street (Lawson Foundation Room)

#### Kitchener Waterloo

* Tuesday, March 26th
* 7:00 p.m.
* Kitchener City Hall (Rotunda)

#### City of Kawartha Lakes

* Wednesday, March 27th
* 6:00 p.m.
* Faith Christian Fellowship Church, 59 Mary St. W.

#### Wallaceburg

* Thursday, March 28th
* 7:00 p.m.
* Royal Canadian Legion Br. 18, 52 Margaret Ave.

#### Sudbury

* Sunday, March 31
* 12:00 – 2:00 p.m.
* Royal Canadian Legion Br. 564, 220 Long Lake Rd.

#### Sault Ste. Marie

* Monday, April 1
* 7:00 p.m.

#### Midland/Penetanguishene

* Wednesday, April 3rd
* 7 p.m.

Oshawa/Durham -- tbc  
Ottawa – tbc  
Kingston – tbc  
Kenora – tbc  
Thunder Bay -- tbc