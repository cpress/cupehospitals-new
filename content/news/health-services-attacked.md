+++
categories = ["News"]
date = "2019-02-07T00:00:00-05:00"
description = "Leaked Ford government plans call for the consolidation of services into 30-50 super hospitals and a massive privatization of clinical and support services, the closure of the LHINs and Cancer Care Ontario. Layoﬀs on a massive level are threatened."
featured = "uploads/g500.jpg"
featuredpath = "img"
title = "Ontario's health care services attacked by Ford government"
type = "post"

+++
# Ford's health care plans leaked: cut, close, restructure, privatize.

_"A militant response is needed to meet the threat to patient care," says OCHU/CUPE"_

* [Click here for members' briefing call details](#briefing-for-local-leadership)

Leaked government plans call for the consolidation of services into 30-50 super hospitals and a massive privatization of clinical and support services, the closure of the LHINs and Cancer Care Ontario. Layoﬀs on a massive level are threatened.

There has been no consultation with any of the organizations representing doctors, nurses or healthcare support staﬀ or with the general public.

The leaked documents set out an aggressive timeline, including forcing legislation by March, with almost no time for public consultation.

The leadership of the Ontario Council of Hospital Unions/CUPE has adopted an action plan to meet the threat and will work with the Ontario Health Coalition and other unions and community groups to push back this plan to sell our healthcare system.

The restructuring of previous governments was not implemented in the face of  vigorous resistance The Harris government tried to privatize land ambulance services and cancer care in the 1990’s. Both of these projects were defeated by on the ground organizing.

Many restructuring and privatization initiatives of the Liberal government were set back by community campaigns.

When the Conservative government campaigned in the 2018 election they did not ask for a mandate for to cut, close, restructure and privatize health services.

Our challenge is to mobilize our membership and the public and to set back these plans to undermine the delivery of healthcare.

We have done it before.  We can do it now.

_The Executive Committee of the Ontario Council of Hospital Unions/CUPE._

***

> ### "A militant response is needed to meet the threat to patient care," says OCHU/CUPE

The ﬁrst step is to inform our members about the threat to the delivery of healthcare services posed by the plans of the PC government.

> [Download the PDF of this newsletter here](https://www.ochu.on.ca/uploads/9/6/7/6/96767358/hsea_news_1.pdf)

### Briefing for local leadership

A teleconference call for local union executive members and stewards will be held on

* Monday February 11
* 7 pm

We will review the restructuring that the government has planned and talk about the actions that the allies of the health care system propose to protect it.

We will provide a teleconference number to all local presidents.

### Members’ Briefing:

All members will be called at home:

* February 19
* 7 p.m.

### Action Plan to Fight the Restructuring:

* OCHU/CUPE will begin the process of consultation with our members about taking militant action to defend the health system
* Together with the healthcare staff represented by other unions we will hold a one day workplace protest at the end of February
* Together with the Ontario Health Coalition, we will mobilize our members and the public into community meetings in March and April
* We will mobilize our members in large numbers to attend a major public protest sponsored by the Ontario Health Coalition on April 30 in Toronto
* We will reach and inform and mobilize every member
* We will meet legislation that strips our basic rights with the appropriate membership response