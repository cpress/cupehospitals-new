+++
categories = ["News"]
date = "2018-11-28T02:31:50+00:00"
description = "Ontario Labour Board finds that the PSLRTA does not preclude or prohibit the implementation of the CUPE Central Agreement Settlement."
featured = "uploads/we won.jpg"
featuredpath = "img"
title = "CUPE WINS CENTRAL DISPUTE - MEMBERS TO GET WAGE INCREASES AND RETRO PAY"
type = "post"

+++
In the spring of 2018 CUPE Locals 1590 and 1144 representing hospital workers at Providence and St Joseph's ratified a Collective Agreement Settlement  that gave CUPE members wage increases of 1.4%, 1.4%, 1.6% and 1.65% over the four year collective agreement.

Just prior to the hospital having to pay the wage increase and retro pay the hospital informed CUPE that they could not implement the negotiated and ratified settlement as it would give an unfair advantage to CUPE over SEIU in the upcoming representation vote.

Today, after months of fighting to have the CUPE Central Collective Agreement Settlement implemented CUPE is excited to announce that the Ontario Labour Relations Board found in CUPE's favour and agreed that 'the PSLRTA does not preclude or prohibit the implementation of the MOS.'

This means that CUPE members at Providence and St Joe's will receive the wage increases and retro pay that was bargained in good faith and ratified by the membership.

CUPE's strength is in the combined effort of its members, the Ontario Council of Hospital Unions (OCHU) and staff that ensures our members rights are upheld.  This is the CUPE advantage and why all workers at St Mike's, Providence and St Joe's should choose CUPE in the upcoming Merger Vote.

Congratulations to all members of CUPE Local 1144 and 1590!
