+++
categories = ["News"]
date = "2018-11-06T21:39:05+00:00"
description = "Ross Memorial chefs strive daily to keep their “employee restaurant” clientele satisfied with fresh, healthy and delicious menu offerings"
featured = "uploads/Cooking at RMH_Super_Portrait.jpg"
featuredpath = "img"
title = "Lindsay Hospital Cooks Up New View On Cafeterias"
type = "post"

+++
When most people think about hospital food or cafeteria food — never mind hospital cafeteria food — it’s unlikely they’d expect to see chimichangas and caesar salads on the menu.

Then again, thanks to Terry Kroonenberg, Paul Cook and Kevin Mack, Ross Memorial Hospital doesn’t have a typical, run-of-the-mill cafeteria.

“It’s an employee restaurant,” said Mack who has been working at the Lindsay hospital for nine years. “We’re always trying to offer something that they \[staff\] might not be able to get here \[Lindsay\], that not only tastes good, but is good for them.”

The three chefs are responsible for developing the daily menu for the cafeteria and for pulling together meals determined by the hospital’s dietitians.

“It’s a very satisfying job,” said Kroonenberg, who quips she “married into” cooking.

Her in-laws owned a fine dining restaurant in Port Perry and it gave Kroonenberg a chance to cut her culinary chops. At the hospital, she is primarily responsible for baking.

“But even as a little kid I’d cook and bake with my grandma. In fact, she really wanted me to be a chef,” said Kroonenberg who has been one for 18 years. “I did a lot of catering too after I married. It’s just something I’ve always loved doing.”

Mack was just 12-years-old when he put on his first set of whites, working at Exhibition Stadium. He later attended George Brown College, at the time, one of only a handful of culinary programs in Canada. He joined the hospital staff in 2009.

Cook began working in a kitchen at age 14. One day, a co-worker said they were leaving to attend culinary school. It sounded like the right move to him too “and away I went.”

Although he hasn’t spent all of his working years slaving over a hot stove, “every time I leave, I’m drawn right back in.”

“I just love food. I always have,” said Cook who has been at the hospital for three years.

The trio are always looking to create something new and tasty for their clientele, including dabbling in international cuisine. On any given day there could be something special from Asia, Mexico or the Mediterranean. It’s always healthy and made with fresh ingredients.

“We’re always getting positive comments from people,” said Mack, noting there’s plenty of cafeterias that can’t say the same thing. “It’s a good feeling. Almost as nice as when we hear our patients say they love our food compared to other hospitals.”

Initially, hospitals prepared their own food. For decades, nurses did double duty in the Ross Memorial kitchen. Then a movement took hold about 20 years ago seeing hospitals outsource food.

According to registered dietician Paule Bernier of Montreal’s Jewish General Hospital, who co-authored a study documenting patient complaints, bringing in food — often cold processed and either reheated or kept chilled for meals — is a smaller budget line. It requires fewer staff and negates the need for a full commercial kitchen.

For cash-strapped hospitals, treating patients, rather than feeding them, was a priority yet recent studies have shown that roughly one-third of medical and surgical patients eat less than half their hospital meals. Some hospitals have recorded statistics as high as 50 per cent.

Ross Memorial already knows that food is medicine for both the staff and patients and has continued to focus on this for years.

But patient meals and covering the cafeteria are really only two parts of the trio’s role at the hospital. Anyone who has attended a special event, meeting or celebration at the Ross Memorial, such as the annual Long-Service Awards or the Volunteer Appreciation Tea, have tasted their talents.

“We do a lot of catering for departments and functions,” said Mack. “It really gives us an opportunity to flex our creative muscles.”

When all these roles are combined, it makes for a pretty busy day.

“From the time you walk in the door, you’re go, go, go. It’s challenging but that’s not necessarily a bad thing . . . Seven-and-a-half hours goes boom,” said Cook.

“It’s all about time management,” said Kroonenberg.

“And the patients come first,” added Mack.

Each of them have a specific "shift" they cover. Cook takes care of the breakfast and lunch belt lines — the meal service for admitted patients — and coming up with a cafeteria lunch entree. Mack covers sandwiches and the main entree for the cafeteria, that could be anything from a stir fry to some type of delicious wrap. Kroonenberg is responsible for all the desserts and goodies for the cafeteria and the dinner belt line.

“I think the best thing of all is that they \[administration\] let us use our own creativity,” said Kroonenberg. “That makes the biggest difference.”

#### by [**Catherine Whitnall**](https://www.mykawartha.com/kawartha-author/Catherine-Whitnall/1e0e6ae6-eb40-4a3c-a4a9-cc957bf70779/)

Catherine Whitnall is a reporter with MyKawartha