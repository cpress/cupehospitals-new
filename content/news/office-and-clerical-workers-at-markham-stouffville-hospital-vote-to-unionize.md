+++
categories = ["News", "Media Release"]
date = "2019-01-23T11:55:46-05:00"
description = "CUPE COMMUNIQUE - Markham Stouffville Hospital Clerical Employees join CUPE"
featured = "uploads/Markham_Stouffville_Hospital-1-1.jpg"
featuredpath = "img"
title = "CUPE Welcomes the Clerical Employees at Markham Stouffville Hospital!"
type = "post"

+++


**MARKHAM, ON** – Office and Clerical workers at Markham Stouffville Hospital have joined the Canadian Union of Public Employees (CUPE) after a certification vote held Tuesday, January 22, 2019. The 257-person unit consists of hospital Receptionists, Secretaries, Clerks, Transcriptionists, Coders and various Clerical positions.

Organizer Deb Oldfield was happy to welcome the group into CUPE. “Our health care system is under threat of cuts and privatization from the Ford government – this could be devastating for the residents of Ontario, who deserve world class health care, and for the people who deliver these services. These workers decided they wanted to have the protection of a CUPE Collective Agreement, as well as equal treatment from their employer and respect for the services they provide. By joining CUPE, they are in a better position to face these uncertain times.”

CUPE represents over 40,000 workers in 60 hospitals in Ontario, making it the largest hospital union in the province. CUPE is committed to ensuring that its members have a strong voice in their workplace and will see no reduction to their working conditions as a result of these mergers.

For more information, please contact:

Deb Oldfield, CUPE Organizing, 905-739-3999

Matthew Stella, CUPE Communications, 613-252-4377
