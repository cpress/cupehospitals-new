+++
categories = ["News"]
date = "2019-07-30T00:00:00-04:00"
description = "CUPE Nurse receives honour for 50 years at Providence Healthcare"
featured = "uploads/Hospitallogoblue.png"
featuredpath = "img"
tags = ["UHT Service", "Unity Health Toronto"]
title = "CUPE Honours Nurse of 50 years July 31 at Providence Healthcare"
type = "post"

+++
**TORONTO, ON** – Elisa Deocampo, a Registered Practical Nurse who has worked for 50 years at Providence Healthcare, will receive special recognition on July 31 at

3:00 p.m. A presentation will take place at 3276 St. Clair Avenue, East Scarborough, Room cc 302.

“Elisa is a wonderful example of dedication to patients and to her co-workers,” says Daniel Callaghan, the president of CUPE Local 1590, which represents staff at the facility. “Ms. Deocampo shows great compassion and demonstrates considerable skill in her nursing. We are incredibly proud of her.”

Lisa Ross, also a Registered Practical Nurse at Providence Healthcare, says, “Very few people love their jobs enough to work at them for more than 50 years. What makes Elisa so special is that she blends a very high level of nursing skill and all of this experience with love for her patients. All of the nursing staff and patients adore her.”

“Think of all of the changes that have taken place in the health care system and in nursing care since 1966. One constant for patients at Providence Healthcare has been Elisa Deocampo, working day in and day out and year in and year out, who has done her absolute best every day to make life better for the people that she cares for. Elisa Deocampo loves her work and we are very grateful to her for her contribution to health care at Providence and in Ontario,” says Michael Hurley, president of the Ontario Council of Hospital Unions/CUPE.
