+++
categories = ["News"]
date = "2019-02-13T00:00:00-05:00"
description = "Government documents leaked to the opposition NDP show that the Doug Ford PCs are planning unprecedented changes to our health care system that would cut, merge, privatize and restructure all aspects of patient care and delivery. Many public health care experts and advocates agree that what the PCs are proposing, and the speed that they intend to carry all this out, will cause great upheaval and pose risks to patients, as care is realigned, cut or privatized."
featured = "uploads/health-plus1.png"
featuredpath = "img"
title = "The PC Government's radical plans to cut, close, restructure and privatize Ontario's health system"
type = "post"

+++
## The government's plans are well underway

Already a super agency that would over- see funding and, ultimately, make key decisions about services in hospitals, long-term care, cancer care, LHINs, community health, addictions, mental health, palliative care, paramedic services, and many more services, was given cabinet approval on January 16.

> [Click here to send a campaign email to your MPP](https://cupe.on.ca/stophealthcareprivatization/)

## The plan is to roll this out quickly

The PC’s intend to begin this restructuring when the Legislature reconvenes on February 19.

Legislation (cynically named the Health System Efficiency Act) to make all this happen has already been drafted. The plan is to proclaim the legislation by July 1.

Thirty to fifty mega health care corporations will be set up across the province, with 10 early adopters established this spring.

## The risks of this rushed, massive experiment are huge

This kind of restructuring threatens chaos in our home care system, the loss of services in small and rural communities, the erosion of cancer care, and the swamping
of community health organizations.

This is all happening before the Premier’s own expert panel tasked with identifying reforms and ending hallway medicine has consulted in any meaningful way with Ontarians and made any recommendations for system change.

## What is threatened?

Public hospitals, particularly those in small and rural communities.

Moving services out of public hospitals and long-term care.

Patient care coordination done by CUPE members employed at Local Health Integration Networks (LHINs). CUPE LHIN members may be the first to experience change.

All hospital support services could be contracted out to private-for-profit companies, most of them large multinationals.

Contracting out of some clinical services. Privatization of air ambulance, laboratories, licensing, and inspections is being actively planned.

Ontarians will have little say and little recourse to challenge decisions made by the government’s super agency.

## What will this super agency have the power to do?

> [_Also see this related fact sheet on the proposed "Super Agency"_](/facts/super-agency-super-power-super-bad-for-patients/)

From the leaked the draft legislation, it is evident the super agency will operate with little public accountability or transparency. The agency, and (especially) the Minister, are given sweeping powers to restructure health care services, close hospitals, and privatize health care services, with little public consultation required.

The super agency will have the power to:

* Consolidate hospital services from 150 hospitals down to 30-50 mega multi-sector health care organizations,
* Absorb functions of the Local Health Integration Networks (LHINs),
* Roll in cancer care services,
* Oversee emergency health care services (e.g. ambulance services),
* Make key decisions about health care infrastructure and spending.

This reform is focused on reducing ser- vices in hospitals and long-term care and increasing profits of private corporations employing low-waged workers.

The approach the Ford PCs are taking is not unlike the last time a PC government closed hospitals and restructured the health system in the late 1990s. Today, we know that patients and front-line health care staff were those hurt most by the previous PC health system restructuring, and Ontario’s Auditor General found that it cost $3.2 billion more than it saved.

> [Download a PDF of this fact sheet here](https://www.ochu.on.ca/uploads/9/6/7/6/96767358/restructure_bulletin_v2.pdf)