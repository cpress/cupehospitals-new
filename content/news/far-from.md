+++
author = "CUPE Hospitals"
categories = ["News"]
date = "2018-07-26T20:29:08+00:00"
description = "Ontario stands to lose 3,712 more hospital beds, more than 16,000 staff under Ford programs. Already funded and staffed at levels well below other provinces, Ontario’s hospitals’ hallway medicine and bed crisis will deepen under Premier Doug Ford’s public service “efficiency” program and promised tax cuts, the Ontario Council of Hospital Unions (OCHU) warned today in Toronto."
featured = "uploads/hall.jpg"
featuredpath = "img"
format = "Hallway Medicine"
title = "Far from ending hallway medicine"
type = "post"

+++
Already funded and staffed at levels well below other provinces, Ontario’s hospitals’ hallway medicine and bed crisis will deepen under Premier Doug Ford’s public service “efficiency” program and promised tax cuts, the Ontario Council of Hospital Unions (OCHU) warned today in Toronto.

OCHU, the hospital division of CUPE in Ontario, has crunched the numbers on three key Ford proposals and their impact on hospitals across the province.

According to OCHU’s report, _Hallway Medicine: It Can Be Fixed_, which looked at the implications of Ford’s promised $7 billion tax plan, balanced budget commitment and a 4 per cent public service “efficiency” program, Ontario’s hospitals would see a loss of 2,467 to 3,712 more beds and between 8,560 to 16,418 jobs.

“We can end hallway medicine by making investments to meet the needs of an aging and growing population. These additional investments are not permanent, but they are needed for the life of the baby boom generation. Ontario’s hospitals, already dealing with overcapacity and years of underfunding, will not be able to maintain the quality of patient care in the face of demographic pressures without these investments,” says OCHU President Michael Hurley.

During this spring’s election campaign, the Conservatives promised to end “hallway medicine” and committed that there would be no public-sector layoffs. However, adding up the revenue and spending cuts across Ontario, thousands of hospital beds and hospital jobs could be cut to meet the target of a balanced budget.

“We have all heard about patients waiting on stretchers in hallways for days. This is a result of the provincial government funding hospitals below their real costs for more than a decade,” said Natalie Mehra, Executive Director of the Ontario Health Coalition. “Ontario has lost more than 18,000 hospital beds in the last 20 years. ‘Hallway medicine’ won’t go away unless the government rebuilds capacity, reopens closed ORs, and restores the staff and funding for beds that have been cut.”

The OCHU/CUPE research makes several recommendations for ending hallway medicine including funding hospitals at their actual costs; opening acute, complex continuing care and long-term care beds to deal with overcrowding; investing in mental health and addictions; and stepping away from restructuring and privatization.

“There is more than enough evidence in Ontario that hospital restructuring and privatization has wasted billions of scarce dollars over the last decade,” says Hurley.