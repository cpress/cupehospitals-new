+++
categories = ["News"]
date = "2019-07-18T00:00:00-04:00"
description = "PC Healthcare cuts mean loss of beds & staff in Kingston"
featured = "uploads/L1974whigpic.jpg"
featuredpath = "img"
tags = []
title = "Hospital union predicts Kingston will lose 28 beds, 242 staff following cuts"
type = "post"

+++
Even as Ontario Premier Doug Ford promised to end hallway medicine within a year at last week’s premiers’ conference, patients at Kingston’s Health Sciences Centre at Kingston General Hospital were being treated in sun rooms.

“Our hospital has been running over capacity for months now,” Barb DeRoche, president of CUPE Local 1974, the representative body for Kingston’s hospital workers, said. “We have people housed in sun rooms, in other rooms, just to give them the care they need.”

Joined by Michael Hurley, president of the Ontario Council of Hospital Unions (OCHU), DeRoche presented a report at Kingston City Hall on Tuesday detailing the effects Ford’s budget cuts to health care could have over the next five years.

The report was compiled over the course of three months using data from the Ministry of Finance, the Canadian Institute of Information, the Financial Accountability Office, Statistics Canada and an independent budget watchdog.

The report predicts a loss of $8 billion in health-care funding over the next five years and a cut of three per cent to hospital funding in this budget year.

“We’re confident in our projections about the very significant budget cuts that are coming to the hospital services here in Kingston,” DeRoche said. “I’m afraid we’re in for some very difficult times ahead.”

Based on these numbers, the two predict Kingston will lose 28 hospital beds and 242 staff over the next five years.

Ontario already has the fewest number of acute care hospital beds out of any province in Canada, according to the Organization for Economic Co-operation and Development (OECD).

Hurley said Kingston hospitals are routinely operating at more than 100 per cent capacity, heightening the risk for medical error and spread of bacteria.

“We’re operating at a level which is quite unsafe,” he added.

While Hurley and DeRoche said they’re confident about their initial projections, they claim Ontario’s aging baby boomer population could deepen Kingston’s health-care losses even further.

“We’re making two predictions, really,” Hurley said. “The first is, there’s going to be an absolute cut as a result of the budget pressures. The second is, when you overlay the demographics on top of the budget cuts, you get a much deeper lack of capacity resulting from the withdrawal of resources at the same time you’ve got an aging and growing population.”

DeRoche said when the extra strain of an aging population is added to the equation, Kingston’s loss of hospital beds over the next five years could amount to 91, with a shortage of 790 staff members.

“We can only end hallway medicine if we get the investments we need to meet the challenges of the aging and growing population,” she said.

DeRoche said if hospitals continue to lose provincial funding, more private entities will set up shop in hospitals, creating what she called a “two-tier” health-care system.

“Those people who have money will be able to get the services, whereas other people in lower to middle-incomes won’t be able to afford the services required,” she said. “It’ll just add to our ongoing wait lists.”
