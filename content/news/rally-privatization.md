+++
categories = ["News"]
date = "2018-10-17T19:02:42+00:00"
description = "Join us to tell Doug Ford that we need to rebuild and improve our public health care: No cuts or privatization. The overwhelming majority of Ontarians oppose health care privatization.  We need to mobilize, to make visible the public demand to improve our public health care. We need to inspire people to stand up and make cuts  and privatization impossible."
featured = "uploads/toronto-rally-top-header.png"
featuredpath = "img"
title = "Rally October 23 at noon in Queen’s Park, Toronto"
type = "post"

+++
Economist Mike Moffatt from Western University has tallied up what Doug Ford has proposed to cut. The total is a whopping $22 billion that Doug Ford proposes in cuts from  provincial revenues over three years. That means that we would lose $22  billion from the money that our province uses to fund health care,  education, roads, transit, and all our social programs. In context, the  Harris government of the 1990s — you remember, the government that  forced mass hospital closures, amalgamations, privatization and cuts —  cut $15 billion (current $) over their first four years. So Ford’s cuts  would be worse if we don’t stop them. (Note: we stopped the Harris cuts  to health care within a few years.)

At the same time, Mr. Ford promised to end hallway medicine. These two sets of promises are contradictory.

One clue to what’s planned came a few weeks ago when Mr. Ford  appointed axeman Gordon Campbell to be his financial guru. Mr. Campbell  is the former B.C. premier who cut and privatized hospitals and their services _en masse_. Under his leadership, user fees for patients  ballooned to the worst in the country. As a result of his policies, B.C.  is ground zero for health care privatization in Canada, two-tier  medicare is rampant, long waits and poor access to care exist throughout  the system and, appallingly, Mr. Campbell is helping the private  clinics in their attempt to bring down public medicare laws in Canada  through a court challenge.

Mr. Ford has no mandate whatsoever  to cut and privatize health care. But given the pro-private anti-public  health care people he is surrounded with, we are deeply concerned that  they will try to foment a fiscal crisis and use it to justify pushing through cuts and privatization that would devastate our public health care.

We all know that we cannot take any more cuts. What we urgently need in our province is to reinvest, to rebuild our health care — not cuts and privatization. 

Sometimes the best defense is a good offense. The best way to protect public health care is to push for what we know we need and to ensure that this government is afraid to cut and privatize health care. We need to set the agenda and make them respond to us.

Please join us. A show of strength is vitally important at this juncture. We  know that the people of Ontario are with us. No one wants more cuts.  The overwhelming majority of Ontarians oppose health care privatization.  We need to mobilize, to make visible the public demand to improve our  public health care. We need to inspire people to stand up and make cuts  and privatization impossible. We can do this. We’ve done it before. But we need your help.

***

### You Can Help! 

* Organize a bus from your local community
* Help to fund a community bus
* Plan and fill a bus from your organization
* Bring out your neighbourhood, faith, seniors’, labour, charity, or service group
* Publicize the rally at your local labour day events
* Come in to our office and help to phone people to come out
* Friend/like us and share our posts on Facebook

[More information is on the Ontario Health Coalition website.](http://www.ontariohealthcoalition.ca/index.php/rally-12-p-m-tuesday-october-23-queens-park-toronto-outside-ontario-legislature-please-join-us-and-tell-doug-ford-that-we-need-to-rebuild-and-improve-our-public-health-care-no-cuts-and-pri/)

[![](/img/uploads/toronto-rally-header.png)](http://www.ontariohealthcoalition.ca/index.php/rally-12-p-m-tuesday-october-23-queens-park-toronto-outside-ontario-legislature-please-join-us-and-tell-doug-ford-that-we-need-to-rebuild-and-improve-our-public-health-care-no-cuts-and-pri/)