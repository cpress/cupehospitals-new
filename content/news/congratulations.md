+++
author = "CUPE Hospitals"
date = "2019-05-09T00:00:00-04:00"
description = "As a result of the overwhelming support of clerical employees at UHT, 348 CUPE members at St. Joseph's Health Centre Toronto will continue to be represented by CUPE. And, approximately 700 non-union clerical employees at Providence Healthcare Toronto and St. Michael's Hospital will now also be represented by CUPE."
featured = "cupe-logo-whitespace.jpg"
featuredpath = "/img/"
format = "Merger Document"
tags = ["Unity Health Toronto","UHT Clerical"]
title = "Welcome to the almost 700 new clerical members at St. Mike’s and Providence"
type = "post"


+++

## CONGRATULATIONS!


As a result of the overwhelming support of clerical employees at UHT, 348 CUPE members at St. Joseph's Health Centre Toronto will continue to be represented by CUPE. And, approximately 700 non-union clerical employees at Providence Healthcare Toronto and St. Michael's Hospital will now also be represented by CUPE.

> **The message to UHT was clear 533 to 168 in favour of CUPE representation.**

Clerical employees were not fooled by the employer’s last-ditch effort to sway workers to vote non-union by sending out an anti-union memo just minutes before the vote started. CUPE has been hearing from clerical workers at St Mike’s, for almost 2 years now, about department restructuring, layoffs and terminations without just cause.

CUPE welcomes all the new members at Unity Health Toronto. You will officially be represented by CUPE once a final decision comes from the Ontario Labour Relations Board which takes approximately 2 weeks.

### LAYOFFS AT UHT

CUPE recently reported on layoffs taking place at UHT. Of those layoffs, 4 were position eliminations for clerical positions at the St Joe’s site, where clerical workers were already CUPE members. As a result of CUPE’s strong job security language these workers have all been offered another position.

Non-union clerical workers at St. Mike’s were not so fortunate and layoffs for these workers were not based on seniority, and contrary to the hospitals reports, workers were not ‘placed’ into new positions but rather were told to apply for vacancies.

Both the employer and the Service Employees International Union (SEIU), took issue with the number of layoffs reported and each put out a memo trying to sway clerical workers to vote non-union. This is behaviour expected from the employer but its unclear why SEIU would take the same anti-union position as the employer.

CUPE has relied solely on reports from non-union workers with regard to any layoffs and terminations. Taking issue with the exact number of layoffs is the employer’s way of diminishing the impact of CUPE’s exposure of job losses at UHT. Shame on SEIU for taking the same position as the employer.

*CUPE - The only choice for workers at UHT*
