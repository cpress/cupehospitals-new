+++
categories = ["News"]
date = "2019-02-26T12:00:00-05:00"
description = "Ontario health care workers have begun a vigorous response. In addition to member mobilizing, CUPE will work with the Ontario Health Coalition, community groups and other labour unions to push back the government restructuring plan. Based on the legislation, the changes the PCs are considering are meant to facilitate the privatization of clinical and support services, the concentration of services from small towns to large urban centres, mega hospitals and add another level of unnecessary bureaucracy to Ontario’s health care system, says the union."
featured = "uploads/superpcattack.jpg"
featuredpath = "img"
title = "Stop the PC’s attack on health system that threatens patient care"
type = "post"

+++
1. [**_Click here to send a letter to Doug Ford and your MPP to stop privatization of health care in Ontario._**](https://www.ontariohealthcoalition.ca/?fbclid=IwAR0MHWNVf8gu_A9Or7KM4b51olbArHIxmcfIDz-ZvtWYii4oFKS9pAi6DRU#safeguard-public-health-care)
2. [**_Download the Ontario Health Coalition's detailed analysis of the Ford announcement._**](https://www.ochu.on.ca/uploads/9/6/7/6/96767358/analysis_of_the_legislation.pdf)

Calling PC government legislation introduced today, “a serious threat to patient care,” the Ontario Council of Hospital Unions/CUPE (OCHU) and the Canadian Union of Public Employees (CUPE Ontario), representing 80,000 hospital, long-term care and home and community care staff, will begin escalating actions province-wide to push the government to reconsider its “radical plans to cut, close, restructure and privatize Ontario’s health system.”

In addition to member mobilizing, CUPE will work with the Ontario Health Coalition, community groups and other labour unions to push back the government restructuring plan. Based on leaked government documents and the legislation, the changes the PCs are considering are meant to facilitate the privatization of clinical and support services, the concentration of services from small towns to large urban centres, mega hospitals and add another level of unnecessary bureaucracy to Ontario’s health care system, says the union.

What we know so far is that, the Conservative government wants to make unprecedented changes to how the public health system operates and delivers patient care. This includes:

* giving the minister of health extra-ordinary powers to privatize hospital services or effectively close hospitals by moving services to another facility;
* a Soviet style “super agency” with special powers to assume control of health services like hospitals, long-term care homes and other health services in communities across Ontario;
* a plan to eliminate homecare coordination now done for patients through Local Health Integration Networks (LHINs);

The plan also imagines the creation of mega health care organizations that will swallow up small and rural hospitals and community health care and social services. All of this is happening with little or no public consultation, yet the legislation tabled today gives sweeping powers to enable “this PC-made-in-Ontario attack on public health care,” says Hahn.

Staff are fierce defenders of the public health care that they deliver. “They have personally soldiered on through decades of downsizing, underfunding, overwork and cuts to both patient care and staffing. Hallway medicine cannot be solved by reducing hospital capacity and privatizing services. Hospital workers will mobilize to protect the public’s access to not-for-profit care, and we believe Ontarians will join us,” says Hurley.

Mobilizing of health care staff began earlier this month with an emergency CUPE membership teleconference to pull health care staff into town hall meetings, rallies and other actions, says Hurley. CUPE is also polling Ontarians’ support for the health system changes the PCs are moving rapidly on.

“There is no time to waste. This dangerous Ford government reboot of our public health system must be stopped before patient care and safety fall victim to the gaps, holes and pitfalls this massive overhaul will create,” says Hahn, who compares the scope of the potential upheaval to the last time a PC government closed hospitals and restructured the health system in the late 1990s.

“It was a failure then. A failure that cost $3.2 billion more to carry out than it saved. Mike Harris eventually had to dramatically increase health care funding and hospital capacity – the cuts were just too deep. After 15 years of real cuts to our health care services and failed experiments with privatization, we cannot allow more cuts and more privatization,” says Hahn.