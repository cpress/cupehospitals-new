+++
categories = ["News"]
date = "2019-03-11T00:00:00-04:00"
description = "CUPE Welcomes Behavioural Support Workers & Behavioural Therapists at West Park Healthcare Centre"
featured = "uploads/westpark.jpg"
featuredpath = "img"
title = "West Park Healthcare Centre workers join CUPE"
type = "post"
tags = [""]

+++
# West Park Healthcare Centre workers join CUPE

**TORONTO, ON** – Behavioural Support Workers and Behavioural Therapists in the Acquired Brain Injury Behavioural Services Department (ABIBS) at West Park Healthcare Centre have joined the Canadian Union of Public Employees (CUPE) after a certification vote held Monday, March 11, 2019.

Organizer Deb Oldfield was happy to welcome the group into CUPE. "These workers decided they wanted to have the protection of a CUPE Collective Agreement, as well as equal treatment from their employer and respect for the services they provide. They also know that our health care system is under threat of cuts and privatization from the Ford government. By joining CUPE, they will have the support of the largest health care union in Ontario and are in a better position to face these uncertain times.”

CUPE represents over 40,000 workers in 60 hospitals in Ontario, making it the largest hospital union in the province. CUPE is committed to ensuring that its members have a strong voice in their workplace and will see no reduction to their working conditions as a result of these mergers.

For more information, please contact:

Deb Oldfield, CUPE Organizing, 905-626-1421

Matthew Stella, CUPE Communications, 613-252-4377
