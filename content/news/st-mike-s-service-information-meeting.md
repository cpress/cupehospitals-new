+++
categories = ["Events"]
date = "2019-02-15T00:00:00-05:00"
description = "CUPE Information Meeting for all St Mike's Service Employees"
featured = "uploads/Feb 21 St Mikes mtg.jpg"
featuredpath = "img"
title = "St Mike's Service Information Meeting"
type = "post"

+++
## CUPE Information Meetings

### St. Mike's Service Employees

* Thursday, February 21
* Pantages Hotel: Refresher Hall 1
* 11am - 2pm  
  And
* 4pm - 8pm

#### Place

Refresher Hall 1  
Pantages Hotel  
200 Victoria Street