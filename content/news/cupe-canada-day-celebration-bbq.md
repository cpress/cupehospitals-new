+++
author = "CUPE Hospitals"
categories = ["Events"]
date = "2019-06-24T00:00:00-04:00"
description = "Join the largest Canadian Union in celebrating Canada Day!"
featured = "/uploads/proudcanpic.jpg"
featuredpath = "/img/"
format = "UHT"
tags = ["Unity Health Toronto", "UHT Clerical"]
title = "CUPE Canada Day BBQ"
type = "post"

+++
Thursday, June 27th

All Service, Clerical and Spiritual Care employees at UHT are invited to come out the CUPE Canada Day BBQ at each of the 3 main sites.

### Providence:

**11:30 a.m. to 2:00 p.m. &  
4:00 p.m. to 6:00 p.m.**  
3266 St Clair Avenue East  
(House beside front entrance)

### St. Joe's:

**10:00 a.m. to 2:00 p.m. &  
4:00 p.m. to 6:00 p.m.**

232 Pearson Avenue  
(just off Sunnyside)

### St. Mike's

**10:00 a.m. to 2:00 p.m. &  
4:00 p.m. to 6:00 p.m.**

Metropolitan United Church

**CUPE the 100% Canadian Grade A union.**