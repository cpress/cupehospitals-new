+++
categories = ["News"]
date = "2018-10-03T17:56:20+00:00"
description = "On the Ontario election campaign trail this summer, Doug Ford vowed to end hallway medicine. Forget it, warns a union representing 30,000 Ontario hospital workers."
featured = "uploads/Hurley pic 2.jpg"
featuredpath = "img"
title = "No end to hallway health care under Ford government, union report warns"
type = "post"

+++
Home to Southwestern Ontario’s largest hospital, London also is ground zero in the region for “hallway medicine,” patients marooned in hospital corridors awaiting treatment in an overcrowded emergency department, or a bed in an overcrowded ward.

On the Ontario election campaign trail this summer, Doug Ford vowed to end hallway medicine.

Forget it, warns a union representing 30,000 Ontario hospital workers.

Ford’s Progressive Conservatives swept into office in June seeking $6 billion in unspecified savings, money to be recovered, the party says, by reducing waste and inefficiencies across all sectors.

That was bad enough for prospects of ending bottlenecks in hospitals, says the Ontario Council of Hospital Unions, the hospital division of the Canadian Union of Public Employees.

But with the rookie government now facing a surprise $15-billion budget shortfall, the chances of a major fix after years of hospital budget freezes under the previous government are slim, Ontario Council of Hospital Unions president Michael Hurley said.

“We’re saying the government really has a moral choice,” Hurley said, adding the situation is only going to worsen as Ontario’s population ages.

“Are we going to meet our obligations with respect to the seniors who are going to be coming to these institutions looking for care, or are we going to turn our backs on them?”

The new report, released at the London Public Library Tuesday morning, says as many as 3,700 hospital beds across the province — including as many as 183 in London — could be on the chopping block if government spending on hospitals is reduced, either by cuts or a spending freeze.

Without those beds and increased investment, Hurley says, hallway health care is here to stay.

In London, the hallway medicine situation is so dire, London Health Sciences Centre instituted a so-called hallway transfer protocol this spring.

The policy sets rules for which patients can be moved to hallways when staffed beds are unavailable.

In her 18 years as a public health-care advocate, Ontario Health Coalition executive director Natalie Mehra said London stands out.

“I have never seen hospitals running at rates of overcrowding that your mental health wards are in London or that other acute care departments in your hospital are. It’s unsafe,” Mehra said. “London’s hospitals are among the worst I’ve ever seen . . . London is an extreme case.”

A spokesperson for Health Minister Christine Elliott dismissed the union’s report.

“CUPE is commenting on this issue purely on the basis of speculation,” Hayley Chazan said by email. “As we have made very clear, one of our key priorities is to end hallway medicine in Ontario.”

The government has committed to creating 15,000 long-term care beds during the next five years and 30,000 beds in a decade, moves that will help alleviate some pressures on hospitals, Chazan said.

“Our government will also continue to listen to the people who plan and work on the front lines of our health-care system as we work to develop a long-term comprehensive health system capacity plan,” she said.

At an address to the Ontario Hospital Association — the professional organization representing the province’s public hospitals — this month, Elliott recommitted to ending hallway health care by innovating and listening to patients first.

It’s not going to happen overnight and will take a lot more than a will to change, New Democrat health critic France Gelinas said.

“Is it possible, within our health-care system the way it is now, to continue to flatline the budget of our hospitals and end hallway medicine? It is not,” she said. “There are changes that could be done to the health-care system as a whole that will have an impact on our hospitals, but not overnight and not in the short term.”

Under the Liberal government, successive years of hospital budget freezes forced institutions to find operational efficiencies on their own, Gelinas said.

The new government’s campaign assertion that it will find enough behind-the-scenes savings to adequately fund front-line care isn’t going to cut it, she said.

“The efficiencies have already been found. They have already been implemented in our hospitals,” she said. “What they need to deal with the overcrowding is more resources to deal with the aging and growing population.”