+++
author = "CUPE Hospitals"
categories = ["News", "Events"]
date = "2019-07-30T00:00:00-04:00"
description = "CUPE's Labour Day event."
featured = "/img/uploads/cupelabday.png"
featuredpath = "img"
tags = ["UHT Clerical", " UHT Service", "Unity Health Toronto", "Events"]
title = "CUPE Labour Day Parade"
type = "itemized"

+++
# BE BOLD. BE BRAVE. DEMAND BETTER.

### MONDAY, SEPTEMBER 2ND

![](/img/uploads/cupelabday.png)

### Join other CUPE members and enjoy a day of solidarity, CUPE swag and free admission to the CNE.

> ### [**_Sign up here_**](https://forms.gle/yroCpHTKsVzqVrzs9 "Labour Day")

#### Meet at University and Armoury (West Side) before 9:30 departure.