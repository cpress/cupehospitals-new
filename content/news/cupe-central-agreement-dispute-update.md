+++
categories = ["News"]
date = "2018-10-11T15:19:44+00:00"
description = "CUPE requests Arbitrator to issue a ruling after Hospital refuses settlement offer.  "
featured = "uploads/image823.png"
featuredpath = "img"
title = "CUPE Central Agreement Dispute Update"
type = "post"

+++
St. Joe’s, Providence and St. Mike’s (Our Shared Purpose) has refused to implement the Central Agreement that was ratified by the Canadian Union of Public Employees (CUPE) and the Hospital.

CUPE filed an Implementation Dispute with the Ontario Hospital Association (OHA) and that dispute was heard by Arbitrator Mitchell on September 24th.  All parties made their arguments to the Arbitrator including the OHA who said that the Central Agreement shouldn’t be implemented because we are in a merger under the Public Sector Labour Relations Transition Act (PSLRTA) and that the Arbitrator did not have jurisdiction to make a decision because it is a PSLRTA matter.  Also, the OHA said that the Central Agreement shouldn’t be implemented for CUPE members at St Joe’s and Providence because it would create an unfair advantage for CUPE in the merger vote against the Service Employees International Union (SEIU).

In an attempt to resolve the matter CUPE suggested, and SEIU agreed, to approach the Hospital and jointly propose extending the Central Collective Agreements, jointly negotiated by CUPE and SEIU, to all members at the Hospital.  This would ensure that going forward after the vote that all employees would have the same rights and protections, promote unity and allow us to move forward from a position of strength.

The Hospital took that proposal to the Hospital Board but the Board did not accept the settlement offer.  Instead the Hospital proposed a two year contract with only the two 1.4% increases.  It had none of the improvements that CUPE achieved, which the hospital agreed to give you, including the two additional 1.6% and 1.65% wage increases in the 3rd and 4th years.

CUPE has not accepted this counter offer.  It is not what we negotiated in good faith and its not what our members ratified.  This is the Hospital’s attempt to put the Hospital’s employees in a position where they would have to negotiate a new collective agreement in this now uncertain political climate.

As there is no settlement, CUPE has asked the arbitrator to rule on our grievance.

If the Arbitrator determines that he does not have jurisdiction then the matter will be dealt with by the Ontario Labour Relations Board (OLRB) and CUPE has already filed the appropriate Unfair Labour Practice (ULP) application with the Board so it will just be a matter of setting a date.  CUPE is determined to do everything necessary to ensure you get the settlement the rest of our members across the Province have received.

As soon as there is more information CUPE will send out an update.  If you have questions you can contact us at [info@cupehospitals.ca](mailto:info@cupehospitals.ca).