+++
categories = ["News"]
date = "2019-05-30T00:00:00-04:00"
description = "Over 120 registered practical nurses, personal support workers, therapists and therapy assistants, social workers, nutrition aides and clerical staff at Hillcrest Reactivation Centre have chosen to improve their working conditions and increase their job security by joining the Canadian Union of Public Employees (CUPE)."
featured = "/uploads/hillcrest.jpg"
featuredpath = "img"
title = "Health Care Workers at Hillcrest Reactivation Centre choose CUPE"
type = "post"

+++
**TORONTO, ON** – Over 120 registered practical nurses, personal support workers, therapists and therapy assistants, social workers, nutrition aides and clerical staff at Hillcrest Reactivation Centre have chosen to improve their working conditions and increase their job security by joining the Canadian Union of Public Employees (CUPE).

Workers cited understaffing, an expansion to the scope of care they are expected to provide and significant inequity with other similar workplaces as the primary motivations for unionizing.

“Health care workers in Ontario know that we are in a period of uncertainty with a government making significant cuts to services that will impact hospital funding, patient care and health care services across the province,” said CUPE Organizer Kristy Davidson. “The best way that workers can protect themselves and stand up for quality health care is by joining a strong health care union that will defend public services and protect workers’ rights.”
