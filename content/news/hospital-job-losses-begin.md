+++
categories = ["News"]
date = "2019-04-07T00:00:00-04:00"
description = "80 positions to be cut at Windsor Regional Hospital"
featured = "uploads/April-30-Rally-Poster1-3.jpg"
featuredpath = "img"
title = "Hospital Job Losses Begin "
type = "post"

+++
# Cuts to Health Care Have Begun

In February there was the announcement that the Grand River Hospital is [cutting 25 full-time and 15 part-time registered nurses]().  Now Windsor Regional Hospital has announced it is [cutting 80 non-union staff positions](https://www.cbc.ca/news/canada/windsor/windsor-regional-fiscal-update-1.5086115?fbclid=IwAR2UxsebAU6E72JfKzzhP5Igqw_dRi16HbL20Gy60emcpxuE_IIXlTwcwA4) in non-clinical roles such as food and housekeeping services.

Service cuts and job loss leading to privatization is the Ontario Premiere's plan for reducing hallway healthcare.

Now more than ever healthcare workers need the protection of a strong CUPE collective agreement with the [best job security language](https://cupehospitals.ca/facts/cupe-job-security-clerical/) of any other union in Ontario.