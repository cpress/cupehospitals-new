+++
author = "CUPE Hospitals"
date = "2019-04-25T00:00:00-04:00"
description = "Times and Locations of Information Meetings and Vote Information"
featured = "uploads/choosecupe.png"
featuredpath = "img"
format = "Clerical"
tags = ["UHT Clerical", "Unity Health Network"]
title = "UHT Clerical Vote & Information Meetings"
type = "post"

+++
## VOTE: May 3rd to May 6th   
 

The Labour Board has determined that the vote for Clerical employees at UHT will be an electronic vote.  **The vote will begin at noon on Friday, May 3rd and ends Monday, May 6th at noon**.  The results of the vote should be available by end of day on May 6th.

You will receive an email to your work email address that contains a link to a secure voting site along with instructions and a help desk number in case you have any issues voting. The vote is secure and the employer will never know how you voted. The benefit of electronic voting is that you will not need to be at work to vote as long as you know how to access your work email from home or from your mobile phone. If you don't know how to access your work email remotetly you can contact your IT department.  You can access your work email at the following link:

#### St. Mike's                    

[https://owa.smh.ca](https://owa.smh.ca "https://owa.smh.ca")  

#### St. Joe’s  

[https://webmail.stjoestoronto.ca](https://webmail.stjoestoronto.ca "https://webmail.stjoestoronto.ca")  

#### Providence  

[https://portal.providence.on.ca/](https://portal.providence.on.ca/ "https://portal.providence.on.ca/")

## **Information Meetings & CUPE Tables**

Prior to the vote the Labour Board allows employees to have access to information about joining the union.  This means that UHT has to allow CUPE to come into the workplace and hold information meetings and set up information tables so you can make an informed decision.  You are allowed to attend these meetings without any worry of reprisal from the employer.

Please come out to one of our information meetings and while enjoying lunch or dinner, speak directly with a CUPE National Organizer who will be able to answer any questions you have about the merger vote and what will happen after the vote. Or, come to one of our CUPE information tables and speak with our organizers, enjoy various treats and enter a draw to win a tablet.

### **St. Mike’s Information Meetings & Tables**

**Tables:**   
daily at the 6th floor Marketeria and lobby of 61 Queen Street

**Information Meetings:**  
April 23rd   4:00pm -6:00pm - Bond 2010

April 24th   11:00am -2:00pm - Pantages Hotel  
                   4:00pm - 7:00pm - Pantages Hotel  
  
 April 26th 11:00am – 1:00pm – Auditorium

April 28th  12:00pm – 2:00pm - 6002 Cardinal Carter

April 29th  7:00am – 9:00am - Bond 3015  
                12:00pm – 2:00pm - Bond 3015

May 2nd 10:00am – 12:00pm - CC6002

### **St. Joe’s Information Meetings & Tables**

**Table:**   
daily in the Cafeteria

**Information Meetings:**  
April 23rd  11:30am - 1:30pm - Conf Rm A/B  
  
 April 26th  4:00pm – 6:00pm - Conf Rm A/B  
  
 April 27th 12:00pm – 2:00pm - Sunnyside Lecture Hall

April 30th    7:00am – 9:00am - Sunnyside Lecture Hall  
                 11:30am – 1:30pm - Conf Rm A/B  
  
 May 2nd  11:00am – 1:00pm - Conf Rm A/B

### **Providence Information Meetings & Tables**

**Table:**   
7am to 7pm in the Cafeteria on April 23rd, 27th, May 3rd   
  
 **Information Meetings:**  
April 25th  11:30am – 1:30pm - CC301

April 28th  11:30am – 1:30pm - CC301/302

May 1st    11:30am – 1:30pm - CC301/302


![](/img/uploads/schedule.png)
