+++
categories = ["News"]
date = "2019-02-01T00:00:00-05:00"
description = "OLRB decision on appropriate bargaining units for Clerks and the OTA/PTA/RA"
featured = "uploads/rawpixel-1055781-unsplash.jpg"
featuredpath = "img"
title = "Labour Board Decision - Unit Clerk & OTA/PTA/RA positions"
type = "post"

+++
Today, January 31, 2019 the Ontario Labour Relations Board (OLRB) issued its Decision regarding which bargaining units the Unit Clerks and the OTA/PTA positions should belong to in the newly formed Unity Health Toronto (UHT).  Excerpts from the OLRB’s Decision appear in italics.

# UNIT CLERKS

The OLRB’s Decision is to put the Unit Clerks in the Office and Clerical bargaining unit.

CUPE currently represents all FT and PT Unit Clerks at St Joseph’s in its Service Bargaining unit and CUPE took the position at the OLRB that they should remain in this unit where they have historically been and not put in the Office and Clerical unit where they run the risk of becoming non-union.

> > _7.         CUPE takes the position that both the Unit Clerk positions and their non-union equivalents at St. Michael’s and Providence should be in the service unit. SEIU and the Employer take the position the Unit Clerks should be in the clerical unit._

UHT took the position that Unit Clerks should be in the Office and Clerical unit.  The obvious reason for UHT to want them in this bargaining unit is in hopes of them ending up as non-union employees.

SEIU, although not representing any Unit Clerks, took the position that the non-union Unit Clerks at St Mike’s and Providence should not be in the Service unit.  There is no principled reason why SEIU would take a position that supports keeping the non-union Unit Clerks from the Service unit.  The only thing that this Decision does is make it more difficult for unionized CUPE clerical to remain unionized.   In addition, it reduces the number of non-union involved in the Service vote that SEIU is involved in.

> > _8.       SEIU takes the position that the non-union equivalents should not be in the service unit and takes no position on whether they should be included in the clerical unit. The Employer objects to their inclusion in the service unit._

# OTA/PTA/RAs

The OLRBs Decision is to put the OTA/PTA/RAs in the Service bargaining unit.

CUPE currently represents OTA/PTA/RAs in its Servicing bargaining unit at Providence and took the position at the OLRB that these positions should remain in the Service bargaining unit and not removed from being able to be in ANY bargaining unit.

UHT took the position that these positions should not be in either the Service or Office and Clerical bargaining unit which would leave these employees with no options and being forced to become non-union.

SEIU also took the same position as the employer.  There is no principled reason for SEIU to take a position that would strip currently unionized employees of their ability to remain in the union.  The only plausible reason for SEIU to take such a position is that it could improve their numerical chances of winning a vote in the Service unit between CUPE and SEIU.

> > _11.       CUPE takes the position that the position of Therapy Assistant should be in the service unit. The Employer and SEIU take the position that it should not be included in that unit, with the effect that the employees occupying the position of Therapy Assistant will lose their bargaining rights._

# The OLRB’s Decision

The OLRBs Decision is great news for OTA/PTA/RAs but disappointing for Unit Clerks.  Although often during a merger unions are pitted against each other, they normally can come together to take principled trade union positions against employers, especially on something as offensive as the employer’s attempt to strip employees of their protection as union members.  CUPE is very disappointed that we could not find an ally in SEIU on these issues.

> > _58.       The Board finds and declares that Unit Clerks and their non-union equivalents are properly included in the clerical bargaining unit, consistent with the purpose of PSLRTA, and that Therapy Assistants are properly included in the service bargaining unit, consistent with the purpose of PSLRTA._
