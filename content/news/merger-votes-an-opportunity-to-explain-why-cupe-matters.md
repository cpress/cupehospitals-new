+++
categories = ["News"]
date = "2018-12-09T14:09:00-05:00"
description = "Hospitals Mergers:  CUPE is the best union for the job."
featured = "uploads/brent_cupe1974.jpg"
featuredpath = "img"
title = "Merger votes an opportunity to explain why CUPE matters."
type = "post"

+++
**Matt Stella** | CUPE Communications


_Brent Tousignant and his executive didn’t get much notice when their workplace, Kingston General Hospital, announced it was going through a merger. But when the announcement came, the Vice-President of CUPE 1974 went into full campaign mode._

“I would say we probably got 15 minutes notice from the employer that they were announcing the merger. Right off the bat we started meeting with people from national and planning what we were going to do leading up to the merger.”

In Ontario, when workplaces represented by multiple unions merge, the process to determine who will represent the workers in the new workplace is called the Public Sector Labour Relations Transition Act or PSLRTA. The legislation was introduced in the 1990s under Mike Harris and has been used to govern mergers under both Progressive Conservative and Liberal governments.

For Brent, the announcement of the merger between Kingston General Hospital and Hotel Dieu was the first he’d heard of PSLRTA. But over the next few months he found himself explaining the very complex details of the legislation to his members and answering many questions about what would happen after the vote.

“I mean it’s confusing for people who go through it. You’re reading the Act, and you’re trying to figure out what’s going on and what’s going to happen after you win or lose. Regardless of who won, most people had practical concerns about what would happen to their seniority, and if they would get bumped out of positions or have to work at multiple sites. It was difficult for us because all we could say to a lot of these questions was ‘the board will decide that later.’”

In order to cut through the confusion about the PSLRTA process and all of the unknowns, Brent and the CUPE 1974 team had a simple message: CUPE is the best union for the job.

“We talked a lot about the strong collective agreement, but also about the resources that you can draw on when you’re a CUPE member—at the local level, at the regional level and at the national level—that give you strength in the hospital sector.”

Brent credits the local’s campaign victory to a lot of hard work by a lot of people. “I just think the support from the executive, the organizers, OCHU, and everyone involved in the campaign—and the amount of time and effort everybody put in—was just phenomenal.”

With a PC government back in power in Ontario there is a possibility for more mergers in the hospital sector and elsewhere. Brent’s advice for locals heading into a merger vote, “Build as strong of a team as possible. We pulled people from other areas, from OCHU, people who had been through this before and were well versed in the issues. It was a tremendous asset.”
