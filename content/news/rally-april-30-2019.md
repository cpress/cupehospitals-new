+++
categories = ["Events"]
date = "2019-02-22T00:00:00-05:00"
description = "Sponsored by the Ontario Health Coalition"
featured = "uploads/rally-ohc-2019-04-23-en-01_orig.png"
featuredpath = "img"
title = "Rally - April 30, 2019"
type = "post"

+++
### OCHU/CUPE carries out advocacy on behalf of our members and on behalf of hospital patients and long-term care residents across Ontario.

OCHU/CUPE is an active partner with the Ontario Healthcare Coalition and works closely with the Ontario Healthcare Coalition whenever community health services are threatened with cuts or privatization.

To read about the PC Government's radical plan to cut, close, restructure and privatize Ontario's health system [click here](http://cupehospitals.ca/admin/#/pages/content-news-the-pc-government-s-radical-plans-to-cut-close-restructure-and-privatize-ontario-s-health-system-md/ "PCs cuts to healthcare").  
​  
​Contact Louis Rodrigues for more information  
[louisr@ochu.on.ca ](mailto:louisr@ochu.on.ca) 613-531-1319

Download posters for your workplace [here](https://www.ochu.on.ca/rallies--more.html "Posters")