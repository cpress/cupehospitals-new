+++
categories = ["News"]
date = "2018-07-25T15:06:02+00:00"
description = "On July 17, 2018 the Ontario Labour Relations Board (OLRB) issued their Decision on the bargaining unit configuration for the newly merged hospitals.  The OLRB determined that there will be one fulltime & part-time Service bargaining unit and fulltime & part-time Clerical bargaining unit."
featured = "uploads/member-update.png"
featuredpath = "img"
title = "CUPE 1144 & 1590 Merger Update"
type = "post"

+++
# OLRB Decision            

 On July 17, 2018 the  Ontario Labour Relations Board (OLRB) issued their Decision on the  bargaining unit configuration for the newly merged hospitals.  The OLRB  determined that there will be one fulltime & part-time Service  bargaining unit and fulltime & part-time Clerical bargaining unit.

 This means that the  CUPE service bargaining units at both St Joe’s and Providence will be  merged with the service bargaining unit at St Mike’s.
 
 The Service unit  at St Mike’s is part of SEIU Local 1 and this means that there will be a  vote of all Service employees at all 3 hospitals between CUPE and SEIU.             

This also means that  the CUPE clerical bargaining unit at St Joe’s will be merged with the  clerical employees at Providence and St Mike’s.  The clerical employees  at Providence and St Mike’s are non-union and this means that there will  be a vote of all Clerical employees at all 3 hospitals between CUPE and Non-union.

**The OLRB has not yet set a Vote Date and a vote won’t take place until the Fall at the earliest.** 

# Central Agreement Implementation

CUPE has filed an Unfair Labour Practice (ULP) complaint at the OLRB due to the Employer refusing to honour the Central Agreement.

# Contact Information

CUPE is collecting your personal contact information to keep you up-to-date  and because the OLRB is now using electronic voting. This means you  will need an email address to be able to vote.

[Click here to update](https://joincupe.org/hospitals/index.php?module=ext/public/form&id=2) CUPE's contact information for you.
