+++
categories = []
date = "2019-02-16T00:00:00-05:00"
description = "Celebrating our members for the outstanding work they do everyday!"
featured = "uploads/images.jpg"
featuredpath = "img"
title = "OCHU Awards 2019"
type = "post"

+++
# The Ontario Council of Hospital Unions (OCHU) is the hospital wing of CUPE.

Our 34.000 members in 70 local unions at 120 facilities bargain a central collective agreement with the Ontario Hospital Association and co-ordinate bargaining across the hospital and long term care sectors.

OCHU is actively involved in patient and resident advocacy in many communities.

Within OCHU there are various committees comprised of members who are elected from the membership across Ontario and whose job it is to ensure members are represented.  Some of those committees include:  RPN Committee, **PSW Committee, Trades Committee, Clerical Committee and the Health & Safety Committee.**

Each year these committees accept nominations for outstanding CUPE members from across the Province.  Please submit your nomination by March 25, 2019.

### RPN of the Year Award

OCHU’s RPN Committee asks you to submit any outstanding RPNs in your workplace as nominees for the RPN of the Year Award.

Please use [this form](https://www.ochu.on.ca/rpn-of-the-year.html?fbclid=IwAR021r4IhMdy7SCcG_IV5ChJDUW57Ljz5BBFwe-KOlBhP7lI0JBNxzJo_Kw "RPN form") or [email us](mailto:admin@ochu.on.ca) the member’s name, workplace, and what makes them an outstanding caregiver.

### PSW of the Year Award

OCHU’s Personal Support Worker Committee asks you to submit any outstanding PSWs in your workplace as nominees for the PSW of the Year Award.

Please use [this form](https://www.ochu.on.ca/psw-of-the-year.html "PSW form") below or [email us](mailto:admin@ochu.on.ca) the member’s name, workplace, and what makes them an outstanding caregiver.

### Tradesperson of the Year Award

OCHU’s Trades Committee asks you to submit any outstanding Tradespeople in your workplace as nominees for the Tradesperson of the Year Award.

Please use [this form](https://www.ochu.on.ca/tradesperson-of-the-year.html "trades form") below or [email us](mailto:admin@ochu.on.ca) the member’s name, workplace, and what makes them an outstanding worker.

### Administrative Support Worker of the Year Award

OCHU’s Clerical Committee asks you to submit any outstanding Administrative Support Workers in your workplace as nominees for the Administrative Support Worker of the Year Award.  
  
Please use [this form](https://www.ochu.on.ca/administrative-support-worker-of-the-year.html "Clerical form") below or [email us](mailto:admin@ochu.on.ca) the member’s name, workplace, and what makes them an outstanding worker.

### Health and Safety Representative of the Year Award

OCHU’s H&S Committee asks you to submit any outstanding H&S reps in your workplace as nominees for the Health and Safety Representative of the Year Award.  
​  
Please use [this form](https://www.ochu.on.ca/health-and-safety-representative-of-the-year.html "H and S form") below or [email us](mailto:admin@ochu.on.ca) the member’s name, workplace, and what makes them an outstanding rep.