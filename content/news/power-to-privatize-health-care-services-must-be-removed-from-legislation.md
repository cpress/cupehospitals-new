+++
categories = ["News", "Media Release"]
date = "2019-03-06T00:00:00-05:00"
description = "Despite assurances from the Ontario PC government that they will not use new health restructuring legislation to privatize services, “key sections of Bill 74 are designed to do just that,” warns Michael Hurley, the president of the Ontario Council of Hospital Unions/CUPE (OCHU). One of those sections explicitly removes reference to keeping hospital and other health services public and not-for-profit. "
featured = "uploads/privatization.png"
featuredpath = "img"
title = "Power to privatize health care services must be removed from legislation"
type = "post"

+++
1. [**_Click here to send a letter to Doug Ford and your MPP to stop privatization of health care in Ontario._**](https://www.ontariohealthcoalition.ca/?fbclid=IwAR0MHWNVf8gu_A9Or7KM4b51olbArHIxmcfIDz-ZvtWYii4oFKS9pAi6DRU#safeguard-public-health-care)
2. [**_Download the Ontario Health Coalition’s detailed analysis of the Ford announcement._**](https://www.ochu.on.ca/uploads/9/6/7/6/96767358/analysis_of_the_legislation.pdf)

Despite assurances from the PC government that they will not use new health restructuring legislation to privatize services, “key sections of Bill 74 are designed to do just that,” warns Michael Hurley, the president of the Ontario Council of Hospital Unions/CUPE (OCHU). One of those sections explicitly removes reference to keeping hospital and other health services public and not-for-profit.

In another section, the health minister is given the power to direct any entity (hospital, long-term care home, LHIN or community agency that receives funding) to utilize company X for a specific service. Documents leaked in January from senior levels of the health ministry revealed plans to privatize air ambulance, nursing home inspections, laboratories, back office and procurement services, among other services.

On top of the system privatizations Bill 74 would permit, more than 20 services, including hospitals, long-term care, Local Health Integration Networks, home care, ambulance, cancer care, organ transplants, palliative care, mental health and addictions are being transferred to a super agency. Ontario’s 150 community hospitals could be shrunk by more than half and mega hospitals created under the Bill. The fate of small and rural hospitals is in doubt.

“We regret that we will have no choice but to ask our members to move into a position to defend the health care system unless the government explicitly abandons privatization. The evidence is clear that the price of health care privatization is increased mortality and that it is much more costly. Turning to private delivery makes a mockery of the pledge to end hallway medicine. There will be even greater access problems and quality-of-care issues. There are some issues, and this is one, where it is important to make a stand,” says Hurley.

With reports that the PC government plans to ram Bill 74 through the legislature in the coming weeks, the Canadian Union of Public Employees (CUPE) is among a growing number of voices calling for extensive public hearings across Ontario on the legislation.

“Ontarians deserve to know every detail of the significant overhaul the PC government intends to make to our public health system. From Toronto to Thunder Bay, they need to be given a chance to comment ahead of any legislation being rammed into law. This is huge restructuring that many experts and front-line staff say is ill-thought-out, risky for patient care, and that could potentially destroy Medicare’s foundations,” says Fred Hahn, president of CUPE Ontario.

Ontario already has the most efficient health system in Canada, spending the least of any of the provinces on hospitals and long-term care.

“It’s not restructuring our health services that’s needed, it’s resources. Long-term care residents need more hands-on care. The risk of this plan causing needless havoc and turmoil is very real and why Ontarians who care deeply about a high-quality public health care system deserve to be heard in communities across the province,” says Heather Duff, CUPE Ontario’s health sector chair.

\-30-

For more information, please contact:

Stella Yeadon  
CUPE Communications  
416-559-9300  
syeadon@cupe.ca