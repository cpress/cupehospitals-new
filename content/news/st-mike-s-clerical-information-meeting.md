+++
categories = ["Events"]
date = "2019-02-15T00:00:00-05:00"
description = "CUPE Information Meeting for all St Mike's Clerical Employees"
featured = "uploads/St Mikes Clerical mtg flyer Feb 20th (1).jpg"
featuredpath = "img"
title = "St Mike's Clerical Information Meeting"
type = "post"

+++
## CUPE Information Meeting

### St. Mike's Clerical Employees

* Wednesday, February 20
* Pantages Hotel: Refresher Hall
* 11am - 2pm  
  And
* 4pm - 8pm

Lunch and/or Snacks Provided

#### Place

Refresher Hall  
Pantages Hotel  
200 Victoria Street