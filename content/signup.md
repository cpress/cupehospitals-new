+++
categories = []
date = "2016-01-01T05:00:00+00:00"
description = "CUPE is a member-driven union. We need your help to make it happen!"
# featured = "uploads/member-update.png"
featuredpath = "img"
title = "Sign-up!"
type = "post"

+++

### There are several ways you can get involved in CUPE's hospital campaigns.

Click on the way 

1. [Sign-up to our email or text message alerts](https://joincupe.org/hospitals/index.php?module=ext/public/form&id=3). These are occasional alerts about a campaign or workplace issue that affects you.
2. [Sign-up to be an activist in your workplace](mailto:info@cupehospitals.ca). An organizer will meet with you and outline the details of the campaign and how you can get involved.
3. [Contact an organizer with a question](mailto:cupeontarioorganizer@cupe.ca) about the campaign in your workplace. We know these campaigns can be confusing for folks. CUPE staff and activists are here to answer your questions and help guide the democratic process.
