+++
author = "CUPE Hospitals"
categories = []
date = "2017-06-22"
description = "If you are unionized and a service employee in a hospital, you will find information here on how mergers in Ontario will affect you."
featured = ""
featuredalt = ""
featuredpath = ""
format = "Information Page"
link = "#"
linktitle = ""
title = "Unionized Hospital Service Workers"

+++
## Unionized Hospital Service Workers

Hospitals and their administrations are merging across Ontario. If you are a hospital service worker, you will be affected by these changes and CUPE is here to help you through these changes.

Unionized service workers in the following hospitals are affected by mergers:

* [St. Michael's Hospital](http://cupehospitals.ca/hospital/stmikes/)
* [St. Joseph's Health Centre (Toronto)](http://cupehospitals.ca/hospital/st-joes/)
* [Providence Health Centre (Toronto)](http://cupehospitals.ca/hospital/providence/)

The hospital merger process in Ontario for unionized workers is managed by Ontario labour law called the [Public Sector Labour Relations Transition Act (PSLRTA)](http://cupehospitals.ca/facts/pslrta/).

#### Things you need to know

1. There will likely be a vote.
2. You will be able to vote on which union (if there is more than one union representing workers at the hospitals) you want to be represented by.
3. There will be lots of information from the unions, management, and the media during this process about seniority, harmonization, bargaining, collective agreement language, and wages & benefits.
4. It is important to be fully informed about the [facts](http://cupehospitals.ca/facts) before voting.

## Detailed information for service workers

1. [CUPE and protecting your job security](http://cupehospitals.ca/facts/cupe-job-security-service/)
2. [Wage harmonization](http://cupehospitals.ca/facts/wage-harmonization-service/)
3. [Hospital merger information](http://cupehospitals.ca/facts/hospital-mergers-service/)
4. [CUPE & Job security ](http://cupehospitals.ca/facts/cupe-job-security-service/)
5. [CUPE - Who we are and what we do](http://cupehospitals.ca/facts/cupe-who-we-are/)
6. [RPNs have a strong voice with CUPE](http://cupehospitals.ca/facts/rpn-info/)
7. [Seniority](http://cupehospitals.ca/facts/seniority-service/)