+++
author = "CUPE Hospitals"
categories = []
date = "2018-08-01T04:00:00+00:00"
description = "If you are a unionized clerical worker, you will find information here about how hospital mergers and other restructuring in the sector will affect you."
featured = "uploads/Clerical-2_3.jpg"
featuredalt = ""
featuredpath = "alt"
format = "Information Page"
link = "#"
linktitle = ""
title = "Unionized Clerical"

+++
## Unionized Clerical in Hospitals

There are many mergers happening between hospitals in Ontario. These mergers affect unionized staff, including clerical workers

<img src="/img/uploads/Clerical-2_3.jpg" align="right">

If you are a unionized clerical worker, it is important for you to understand the process, what is at stake, and what you can do to affect the process.

It is likely that you will have the opportunity to vote on your union representation at some point in the process. The following is a list of resource that you can use to find out information on that process:

1. Review [CUPE's detailed review of the hospital merger process](http://cupehospitals.ca/facts/pslrta/) and how it affects you.
2. Review information specific to [your hospital](http://cupehospitals.ca/hospital/). The process will be different depending on the hospital involved.
3. Review information specific to you as a unionized hospital clerical worker here on CUPE Hospitals.ca.
4. Learn more about [CUPE](http://cupehospitals.ca/about/), Canada's largest health care union.
5. Make an informed vote come vote day.

## DETAILED INFORMATION FOR CLERICAL

1. [Mergers - The composite collective agreement](http://cupehospitals.ca/facts/hospital-mergers-clerical/)
2. [CUPE - Who we are](http://cupehospitals.ca/facts/cupe-who-we-are-clerical/)
3. [CUPE & Job Security](http://cupehospitals.ca/facts/cupe-job-security-clerical/)
4. [Wage Harmonization](http://cupehospitals.ca/facts/wage-harmonization-clerical/)
5. [What to expect from your employer](http://cupehospitals.ca/facts/managements-message/)
6. [Seniority](http://cupehospitals.ca/facts/seniority-clerical/)
