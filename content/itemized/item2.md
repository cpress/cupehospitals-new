+++
author = "CUPE Hospitals"
categories = []
date = "2017-06-22"
description = "Clerical workers in hospitals who do not currently belong to a union will find information here that will help explain how the current restructuring will affect you."
featured = ""
featuredalt = ""
featuredpath = ""
format = "Information Page"
link = "#"
linktitle = ""
title = "Non-union Clerical"

+++
## Non-union Clerical

If you are a clerical worker who is not currently in a union, you are still affected by the merger of hospitals in downtown Toronto.

The following are a list of locations that have non-union clerical workers:

* [St. Michael's Hospital](http://cupehospitals.ca/hospital/stmikes/)

## St. Michael's Hospital

There is currently a restructuring of hospital administration and management at sites in downtown Toronto, including [St. Michael's hospital.](http://cupehospitals.ca/hospital/stmikes/)

If you are working as a clerical worker at St. Michael's you are affected.

#### Here is what you need to know

1. St. Michael's is merging with St. Joseph's Health Centre and Providence Health Centre. This will affect employment at your hospital and who you report to.
2. St. Joseph's and Providence clerical employees are members of the Canadian Union of Public Employees (CUPE).
3. The merger process outlined in Ontario labour law means you have the right to vote on whether all workers will become non-union clerical **OR** for all workers across the three sites become unionized.
4. During this vote, you will have the opportunity to vote to be represented by the Canadian Union of Public Employees (CUPE) and be covered by our collective agreement.

***

# Detailed information for clerical

1. [CUPE & Job Security](http://cupehospitals.ca/facts/cupe-job-security-clerical/)
2. [Wage Harmonization](http://cupehospitals.ca/facts/wage-harmonization-clerical/)
3. [Joining a union](http://cupehospitals.ca/facts/joining-union/)
4. [How does my workplace become unionized?](http://cupehospitals.ca/facts/how-union/)
5. [What to expect from your employer](http://cupehospitals.ca/facts/managements-message/)
6. [Seniority](http://cupehospitals.ca/facts/seniority-clerical/)
7. [CUPE - Who we are](http://cupehospitals.ca/facts/cupe-who-we-are-clerical/)