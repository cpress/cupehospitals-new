+++
author = "CUPE Hospitals"
date = "2018-01-01"
description = "Toronto, Ontario"
featured = "chaplains.jpg"
format = "Merger"
title = "St. Michael's Hospital"
type = "itemized"

+++
![](/img/2018/03/chaplains.jpg)

## St. Mike's Hospital Merger Update

The Ontario Labour Relations Board (OLRB) has made a decision that there will be **two separate bargaining units** in the newly merged Our Shared Purpose hospital:

1. A bargaining unit of full-time & part-time for [**servicing staff**.](http://cupehospitals.ca/itemized/item3/)
2. A bargaining unit of full-time & part-time for [**clerical staff**](http://cupehospitals.ca/itemized/item1/).

For clerical members this means that CUPE clerical at St. Joe's will be merged with non-union clerical from St Mike’s and Providence and will be in a vote between CUPE and non-union.

For service members this means the CUPE service at St. Joe's and Providence will be merged with service members from St. Mike's and will be in a vote between CUPE and SEIU, an American union.

There is still a dispute about which unit the Unit clerks at St. Joe's and Ward Aides at Providence should be in.  Currently they are in the CUPE service units at both St. Joe's and Providence and are non-union at St. Mike's.   CUPE is taking the position that they should remain in the new service unit.  SEIU and the employer are taking the position that they should be in the clerical unit putting them at a greater risk of ending up non-union.  This issue will have to be decided by the OLRB before a vote is held.

### What does this means for the vote?

You will have the option of voting for being unionized with CUPE or remaining non-union.

While the OLRB has not set a vote date yet, a vote may take place in Fall.

With the merger vote comes the opportunity for you and your colleagues to join CUPE. If you vote to join CUPE you will benefit from the same strong collective agreement that other CUPE members have including job security and protection from being unjustly disciplined or terminated.

CUPE will be holding information meetings and you will start to see our organizers in St Mike’s who will be able to answer your questions.

For further information and to make sure to get merger updates please speak to a CUPE organizer at info@cupehospitals.ca or use the contact link at the top of the page.

***

* [News for St. Mike's.](http://cupehospitals.ca/tags/st-michaels/)
* For more detailed information, see the section of [fact sheets](http://cupehospitals.ca/facts/).

## ADDITIONAL INFORMATION

* [CUPE Part-time members](http://cupehospitals.ca/facts/cupe-part-time-members--service/ "Part time fact sheet")
* [CUPE Part-time 14% advantage](http://cupehospitals.ca/facts/cupe-14-cupe-advantage--service/ "14% pt fact sheet")
* [Wage Harmonization](http://cupehospitals.ca/facts/wage-harmonization-clerical/)
* [Promotion & Posting](http://cupehospitals.ca/facts/post-promotion/)
* [CUPE's Superior Job Posting Language](https://gkauq-b8elwdja.preview.forestry.io/facts/job-postings-service/ "Job Posting fact sheet")
* [CUPE’s detailed guide to PSLRTA](http://cupehospitals.ca/facts/pslrta/)
* [I am not in a union, what should I do?](http://cupehospitals.ca/facts/how-union)
* [Why choose CUPE?](http://cupehospitals.ca/about/)
* [Nursing News](http://cupehospitals.ca/newsletter/nurse-june19)
* [Clerical News](http://cupehospitals.ca/newsletter/clerical-july/)
* [PSW News](http://cupehospitals.ca/newsletter/psw-july/)