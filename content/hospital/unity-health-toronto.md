+++
author = "CUPE Hospitals"
date = "2019-01-15T16:39:05+00:00"
description = "Toronto, Ontario"
format = "Merger"
title = "Unity Health Toronto"
type = "itemized"
link = "/uht/"
linktitle = ""

featured = ""
featuredpath = ""
featuredalt = ""

tags = ["Unity Health Toronto"]

+++


On August 1, 2017 Providence Healthcare, St. Joseph’s Health Centre and St. Michael’s Hospital merged together to become one new hospital, Unity Health Toronto (UHT).

<center>
<figure>
  <img src="/img/uploads/unity-health-toronto-header.jpg" width="75%" />
    <figcaption>
	<h4>The new logo of Unity Health Toronto and partner hospitals</h4>
	</figcaption>
</figure>
</center>
## Mergers


The merger of St. Joeseph's, St. Michael's, and Providence into UHT began changing how it provided services and this meant that work historically done by health care workers at the three different sites who are represented by various unions, started to change and merge.

The first sign of changes came with the crossover of work between the labs at St. Mike’s and St. Joe’s which resulted in LiUNA filing an application under the Public Sector Labour Relations Act (PSLRTA) in the fall of 2017.
PSLRTA is the legislation used during the merger of a Hospital that sets out how process for determining what the new workforce structure will be.  It is the process that determines what employee bargaining units will be established at the newly merger hospital as well as the process for determining which union, if any, will represent the workers.

## Bargaining Units

Among several bargaining units determined by the Labour Board, the Board issued a Decision that there should be one Service bargaining unit and one Clerical Bargaining unit. 

- UHT clerical employees are either represented by CUPE or are non-union
- UHT service employees are represented by CUPE and SEIU.

## Votes

The make-up of the bargaining units means that there will be two votes held by the Labour Board. 

1. To determine whether clerical employees wish to be represented by CUPE.
2. To determine if service employees wish to be represented by CUPE or SEIU.

This site will provide you with detailed information about the merger and PSLRTA process as well as the protections provided by a CUPE collective agreement as the merger and restructuring process unfolds.


