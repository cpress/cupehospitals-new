+++
author = "CUPE Hospitals"
date = "2019-01-20T17:13:26-05:00"
description = "Markham, Ontario"
format = "Unionization"
title = "Markham Stouffville Hospital"
type = "itemized"

+++
## Certification Application filed!

![](/img/uploads/Markham_Stouffville_Hospital-1.jpg)

CUPE filed the Application for Certification for all full-time, part-time and casual Office and Clerical Employees at Markham Stouffville Hospital on January 15th.

The vote will take place on Tuesday, January 22nd.  Poll times are:

7:00 a.m. to 9:00 a.m.  -  Room A-2317

2:30 p.m. to 4:30 p.m. - Room A-2516

The voting process is completely confidential and the employer will never know how you voted.

The Ontario Labour Relations Act ensures you the right to choose to join a union and to do so free from harassment, intimidation or coercion.  If you or any of your colleagues experience any behaviour from your managers or supervisors that you feel is inappropriate please contact Deb Oldfield, CUPE Organizer at [doldfield@cupe.ca](mailto:doldfield@cupe.ca) or call 905-626-1421.

CUPE Information information pages about unionization:

* [Know the Facts - Join CUPE](http://cupehospitals.ca/facts/know-the-facts-clerical-employees-at-markham-stouffville-hospital/)
* [How does my workplace become unionized with CUPE?](http://cupehospitals.ca/facts/how-union/)
* [Joining a Union: It is so much more than wage increases](http://cupehospitals.ca/facts/joining-union/)
* [What to expect from your Employer](http://cupehospitals.ca/facts/managements-message/)
* [Learn more about CUPE](http://cupehospitals.ca/about/)
