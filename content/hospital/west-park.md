+++
author = "CUPE Hospitals"
date = "2018-07-19T22:56:09+00:00"
description = "Toronto, Ontario"
format = "Unionization"
title = "West Park Healthcare Centre"
type = "itemized"

+++
CUPE is currently organizing Rehabilitation and Behavioural Therapists at West Park.

![](/img/uploads/West_Park_548x307.jpg)

# Join CUPE

CUPE is Ontario's hospital union. We represent over 40,000 workers in 120 hospitals across the province.

Joining CUPE will improve your working conditions. CUPE members have the strongest job security provisions of any hospital workers in Ontario. We have also recently campaigned successfully to bring our members increased protections against violence in the workplace.

CUPE is a democratic union that puts its members first. As a CUPE member you will have a strong voice in your workplace and in your union.

[Please fill in this form](https://joincupe.org/hospitals/index.php?module=ext/public/form&id=2) to contact a CUPE Organizer to sign a union card or if you have any questions.

Or, have a read of our information pages about unionization:

* [How does my workplace become unionized with CUPE?](http://cupehospitals.ca/facts/how-union/)
* [Joining a Union: It is so much more than wage increases](http://cupehospitals.ca/facts/joining-union/)
* [What to expect from your Employer](http://cupehospitals.ca/facts/managements-message/)
* [Learn more about CUPE](http://cupehospitals.ca/about/)