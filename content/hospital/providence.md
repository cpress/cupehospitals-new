+++
author = "CUPE Hospitals"
date = "2018-01-01"
description = "Toronto, Ontario"
format = "Merger"
title = "Providence Hospital"
type = "itemized"

+++
## Merger Update

The Ontario Labour Relations Board (OLRB) has made a decision that there will be **two separate bargaining units** in the newly merged Our Shared Purpose hospital:

1. A bargaining unit of full-time & part-time for [**servicing staff**.](http://cupehospitals.ca/itemized/item3/)
2. A bargaining unit of full-time & part-time for [**clerical staff**](http://cupehospitals.ca/itemized/item1/).

For service members this means the CUPE service at  Providence and St. Joe's will be merged with service members from St. Mike's and will be in a vote between CUPE and SEIU, an American union.

For clerical members this means that CUPE clerical at St. Joe's will be merged with non-union clerical from St Mike’s and Providence and will be in a vote between CUPE and non-union.

There is still a dispute about which unit the Unit clerks at St. Joe's and Ward Aides at Providence should be in.  Currently they are in the CUPE service units at both St. Joe's and Providence and are non-union at St. Mike's.   CUPE is taking the position that they should remain in the new service unit.  SEIU and the employer are taking the position that they should be in the clerical unit putting them at a greater risk of ending up non-union.  This issue will have to be decided by the OLRB before a vote is held.

### The CUPE advantage

CUPE has the best Central Collective Agreement of any other health care union in Ontario. CUPE has the best job security language, the best job posting language, 100% success rate bringing wages to the highest rate after a merger vote and 14% in lieu of benefits for part-timer employees. Read the [CUPE fact sheets](http://cupehospitals.ca/facts/ "CUPE fact sheet main page") and make sure you know what it means to have the CUPE advantage.

### CUPE Local 1590 has represented employees at Providence Healthcare since 1973.

Since then the Local has expanded to include employees of DTZ who also work at Providence Healthcare as well as employees at Hazelton Retirement Residence. Currently the membership includes 700 fulltime, part time and casual workers.

Hospital workers have seen more than our share of change in the workplace. Hospital mergers similar to the one between the Providence and St. Joe's and St. Mikes are happening in several Ontario communities. While there will be many workplace and organizational transitions, the merger also brings changes at the local CUPE union and bargaining unit level.

The Local’s first priorities are to negotiate strong collective agreements and protect the rights of our members. As a CUPE Local that is part of CUPE’s Ontario Council of Hospital Unions (OCHU) we are part of CUPE’s Central Agreement and have the strongest job security and job promotion language of any health care union in Ontario. In addition, we have negotiated wage increases and lump sum payments in each year of our collective agreement.

> If the employer violates the collective agreement grievances are filed and CUPE Local 1590 and OCHU have had many successes over the years including payment for damages for breach of the collective agreement.

Our Local is an active local so in addition to negotiating strong collective agreements and enforcing those agreements, the Local also ensures that issues of importance to the members such as violence in the workplace, health and safety, workload, creation of new jobs and semi-annual vacation planners are supported by the Local Executive members with achievable outcomes for our members. Beyond this the local supports and provides education opportunities for members and social events such as CUPE day celebrations.

### Additional Information

* [Harmonization Process](/facts/harmonization/)
* [Promotion & Posting](/facts/post-promotion/)
* [CUPE's detailed guide to PSLRTA](/facts/pslrta/)
* [I am not in a union, what should I do?](/facts/how-union)
* [Why choose CUPE?](/about/)
* [Nursing News](/newsletter/nurse-june19)
* [Clerical News](/newsletter/clerical-july/)
* [PSW News](/newsletter/psw-july/)