+++
author = "CUPE Hospitals"
date = "2018-07-11T19:44:34+00:00"
description = "Toronto, Ontario"
format = "Merger"
title = "St. Joseph's Health Centre"
type = "itemized"

+++
## Merger Update

The Ontario Labour Relations Board (OLRB) has made a decision that there will be **two separate bargaining units** in the newly merged Our Shared Purpose hospital:

1. A bargaining unit of full-time & part-time for [**servicing staff**.](http://cupehospitals.ca/itemized/item3/)
2. A bargaining unit of full-time & part-time for [**clerical staff**](http://cupehospitals.ca/itemized/item1/).

For clerical members this means that CUPE clerical at St. Joe's will be merged with non-union clerical from St Mike’s and Providence and will be in a vote between CUPE and non-union.

For service members this means the CUPE service at St. Joe's and Providence will be merged with service members from St. Mike's and will be in a vote between CUPE and SEIU, an American union.

There is still a dispute about which unit the Unit clerks at St. Joe's and Ward Aides at Providence should be in.  Currently they are in the CUPE service units at both St. Joe's and Providence and are non-union at St. Mike's.   CUPE is taking the position that they should remain in the new service unit.  SEIU and the employer are taking the position that they should be in the clerical unit putting them at a greater risk of ending up non-union.  This issue will have to be decided by the OLRB before a vote is held.

<iframe width="80%" src="https://www.youtube-nocookie.com/embed/3kiQkgxepQk?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

### The CUPE advantage

CUPE has the best Central Collective Agreement of any other health care union in Ontario. CUPE has the best job security language, the best job posting language, 100% success rate bringing wages to the highest rate after a merger vote and 14% in lieu of benefits for part-timer employees. Read the [CUPE fact sheets](http://cupehospitals.ca/facts/ "CUPE fact sheet main page") and make sure you know what it means to have the CUPE advantage.

### Hospital Mergers

Hospital workers have seen more than our share of change in the workplace.

[Hospital mergers](facts/pslrta) similar to the one between the Providence  and St. Joe’s and St. Mikes are happening in several Ontario communities.  While there will be many workplace and organizational transitions, the  merger also brings changes at the local CUPE union and bargaining unit  level.

The Local’s first priorities are to negotiate strong collective  agreements and protect the rights of our members. As a CUPE Local that  is part of CUPE’s Ontario Council of Hospital Unions (OCHU) we are part  of CUPE’s Central Agreement and have the strongest job security and job  promotion language of any health care union in Ontario. In addition, we  have negotiated wage increases and lump sum payments in each year of our  collective agreement.

Choosing a union to represent you in the workplace is extremely important. At CUPE, we have dedicated staff and member-organizers to be there for you, through every step of the PSLRTA process.

CUPE has an excellent track record of protecting and making gains for our members.

* CUPE is Ontario’s largest union with over 75,000 CUPE members working in health care.
* CUPE offers vast experience and rich expertise in negotiating superior collective agreements in the hospital sector.
* CUPE negotiates to maintain superior protections and improve working conditions.
* CUPE’s hospital sector collective agreement job security and job posting provisions are the strongest of any hospital union in Ontario.
* CUPE is an activist union.
* CUPE proudly advocates and campaigns to protect public health care.

# CUPE 1144

CUPE local 1144 has over 750 members that represent the full-time and part-time service and clerical staff of St. Joseph’s Health Centre. We are passionate about the quality care we provide to the community in the west-end of Toronto.

Within our Service bargaining unit we represent people employed in many classifications including: Building systems operators, Refrigerator and Controls specialists, Electricians, Plumbers, Mechanics, Registered Practical Nurses, Carpenters, Painters, Finishers, Addictions services workers, Bakers, Cardio-Respiratory Assistants, Cooks, Health Care Attendants, Storekeepers, Unit clerks, Custodians, Porters, Cleaners, Dietary aides and many others.

Our Clerical bargaining unit represents people employed in various positions including: Health Records technicians, Bed Booking clerks, O.R. booking clerks, Payroll clerks, Medical Transcriptionists, Diet and Nutrition Technicians, Audit clerks, Registration clerks, Switchboard operators, Secretaries, Purchasing assistants, Finance clerks, and clerical staff in many other departments.

Our collective agreements form the regulations that both the employer and the employees have agreed to. Both parties are bound by this document. Your union executive will work hard to ensure that the collective agreement is followed and will assist you with any disputes that you may have following the grievance procedure. (for more info see the section on this website on grievances)

It takes many dedicated people to keep a hospital running. We are proud to  provide excellent quality healthcare to our local community.

All members are welcome to attend our General Membership meetings held on the 4th Wednesday of the month at 4p.m. in Classroom 5 Sunnyside building.