+++
author = "CUPE Hospitals"
date = "2018-08-28T15:03:11+00:00"
description = "Peterborough, Ontario"
draft = true
format = "Merger"
title = "Peterborough Regional Health Centre"
type = "itemized"

+++
![](/img/uploads/Peterborough Regional.jpg)

### Peterborough Regional Health Centre Merger Update

Right now we don't have a lot of firm details about what will happen in terms of a merger involving Peterborough Regional. It is very likely that Peterborough Regional will merge with Ross Memorial Hospital in Kawartha Lakes. It is also possible that other hospitals could be involved in this merger, but there has been no decision from the provincial government at this time.

### What does this means for workers at Peterborough Regional?

When hospitals merge and the workers at each workplace are representated by different unions it results in a representation vote as governed by the [Public Sector Labour Relations Act (PSLRTA)](http://cupehospitals.ca/facts/pslrta/). Once the government has decided which hospitals will be involved in the merger we will know if there will be a representation vote. If one union represents 80% or more of the workers in the workplace, there will be no representation vote. If there are two or more unions that each represent at least 20% of the workers in the merged workplace then there will be a representation vote. We will continue to update this website as new information about the merger becomes available.

CUPE 1943 members can sign up for updates about the merger and local bargaining issues [here](https://joincupe.org/hospitals/index.php?module=ext/public/form&id=2).

### Resources for understanding the merger process

For more detailed information about the meger process please look at CUPE's detailed fact sheets to help you understand what a merger could mean for your Collective Agreement, Seniority, Job Security and Job posting.

[CUPE PSLRTA Factsheets](http://cupehospitals.ca/facts/ "CUPE PSLRTA Factsheets")