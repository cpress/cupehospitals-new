+++
author = "CUPE Hospitals"
date = "2019-03-11T00:00:00-04:00"
description = "Last week on March 5th CUPE was at the Labour Board with the Hospital to try and resolve the issue of which clerical positions will be in the new clerical bargaining unit. Under the Public Sector Labour Relations Transition Act (PSLRTA) the Act requires that only positions that are similar to those that already exist in a union, will be able to be brought into the new bargaining unit."
format = "Merger Update, Clerical"
title = "UHT Merger Update: Clerical"
featured = "/img/uploads/member-update-small.png"
featuredpath = "/"
featuredalt = ""
tags = ["Unity Health Toronto","UHT Clerical"]

+++

## MERGER UPDATE:  UHT CLERICAL BARGAINING UNIT

Last week on March 5th CUPE was at the Labour Board with the Hospital to try and resolve the issue of which clerical positions will be in the new clerical bargaining unit. Under the Public Sector Labour Relations Transition Act (PSLRTA) the Act requires that only positions that are similar to those that already exist in a union, will be able to be brought into the new bargaining unit.

In this case with UHT only the clerical at St Joe’s are unionized and the clerical at Providence and St Mike’s are non-union. This means that when you apply PSLRTA, only the non-union positions that are essentially similar to those already unionized at St Joe’s will be part of the new bargaining unit.

For the most part CUPE and the hospital have been able to agree on which positions are to be included but there are few that remain in dispute. Attached you will find a list by site of which positions are included in the new bargaining unit, which ones are excluded and the ones that remain in dispute. It is the position of the hospital, on all of the positions that remain in dispute, that they do not belong in the new bargaining unit. In some cases we are awaiting further information on these jobs such as job descriptions and/or job postings.

# **Who Can Vote?**

If you hold a position within any of the classifications that are listed on the lists below as being ‘in’ the new bargaining unit then you will be entitled to vote when the merger vote takes place.

If you hold a position within any of the classifications that are listed on the lists below as being ‘in dispute’ then you will be entitled to vote but your vote will be segregated. This means that you can cast your electronic vote but that it will not be included in the overall count until it is determined whether the position you hold will be in the new bargaining unit. If CUPE and the employer can’t agree on the disputed positions then the Labour Board will make the final decision.

If you have questions you can contact your local executive or email us at [info@cupehospitals.ca](mailto:info@cupehospitals.ca).

### ST MIKE'S

**ST MIKE'S - POSITIONS IN NEW BARGAINING UNIT**

* Accounts Payable Clerk
* Billing Clerk
* Booking Clerk
* Call Centre Agent
* Cashier
* CATCH Coordinator
* Clerical Assistant - Clinics
* Clerical Assistant - In Patient
* Clerk
* Coordinator - Security Services
* Creative Works Studio Assistant
* Data Entry Clerk
* Diagnostic Labs Reporting Assistant
* Dietitian Assistant
* Dispatch Clerk
* Facility Operations Assistant
* Health Record Clerk
* Information Desk Assistant
* Ingredient Control Clerk
* Library Assistant
* Logistics Assistant - Greeting & Distribution
* Medical Imaging Clerk
* Menu Clerk
* Patient Accounts Representative
* Patient Registration Clerk
* Scheduling & Events Administrator
* Senior Admitting Clerk
* Stores Clerk
* System Coordinator - Health Records
* Team Lead Locating Call Center

**ST MIKE'S - POSITIONS OUT OF NEW BARGAINING UNIT**

* Admin Assistant - CLM
* Admin Assistant - Department
* Admin Assistant - Department Chief
* Admin Assistant - Director
* Admin Assistant - HCDC (External)
* Admin Assistant - Manager
* Admin Assistant - Medical Leadership
* Admin Assistant - Medical Leadership Support
* Admin Assistant - Physician
* Admin Assistant - Physician (Clinical Services)
* Admin Assistant - Surgeon In Chief
* Backup & Storage Administrator
* Buyer
* Coordinator - Client Services
* Health Records Quality Coordinator
* HR Service Center Assistant
* Infection Prevention Auditor
* Manager, Giftshop
* Executive Assistant
* Executive Assistant - EVP
* Executive Assistant - Executive and Board Committees
* Executive Assistant - Vice President
* Graphic Artist
* Job Description Assistant
* Jr. Recruitment Rep
* KR Summer Student Program Coordinator
* Logistics Assistant - Information Desks
* Operations & Quality Analyst
* Payroll Analyst
* Research Administrative Assistant
* Research Contracts Clerk
* Sales Associate - Giftshop
* Student Center Systems Coordinator
* Systems Support Administrator
* End User Support Specialist

**ST MIKE'S - POSITIONS IN DISPUTE**

* Community Support Worker
* Telecommunications Specialist
* Peer Support Specialist
* Facilities Coordination Administrator-
* Data Coordinator- Dialysis
* Admin/Clerical Coordinator
* Release of Information Specialist
* Breast Centre Activities Specialist
* Admin Assistant TC LHIN
* Fit Tester - fits masks - RPN other site

### 

### PROVIDENCE

**PROVIDENCE - POSITIONS IN NEW BARGAINING UNIT**

* ACCOUNTS PAYABLE REPRESENTATIVE
* ADMINISTRATIVE ASSISTANT - DIAGNOSTIC SERVICES
* ADMINISTRATIVE ASSISTANT 1
* ADMINISTRATIVE ASSISTANT II
* ADMINISTRATIVE ASSISTANT, STAFFING OFFICE
* ADMINISTRATIVE ASSISTANT,PCU
* ADMINISTRATIVE COORDINATOR, RESIDENT CARE
* ADMISSION DISCHARGE & TRANSFER COORDINATOR
* CSR, SWITCHBOARD
* CSR, SWITCHBOARD TM LD
* CUSTOMER SERVICE REPRESENTATIVE/BILLING
* UNIT ADMINISTRATIVE COORDINATOR, PCU
* ADMINISTRATIVE COORDINATOR
* DIETITIAN ASSISTANT
* Menu Clerk
* info Perm Coordin
* Health info Mgt Prof
* ADT Coord admitting admin

######   
PROVIDENCE - POSITIONS OUT OF NEW BARGAINING UNIT

* EVENTS ASSISTANT
* EXECUTIVE ASSISTANT, M&V
* EXECUTIVE ASSISTANT, VP CORP SVCS & VP FINANCE
* GOVERNANCE AND CORPORATE MANAGER

######   
PROVIDENCE - POSITIONS IN DISPUTE

* PAYROLL ADMINISTRATOR

### **ST JOSEPH'S**

**ST JOE'S - POSITIONS IN NEW BARGAINING UNIT**

* Bed Booking Clerk
* Booking/Computer Clerk
* D.I. Clerk
* Data Quality Audit Clerk
* Diet Office Clerk
* Dietetic Technician
* Differential Co-ord.
* Electronic Health Records Clerk
* Family Medicine Clerk
* Finance Clerk
* Health Records & Transcription Clerk
* Health Records Clerk
* Health Records Tech - ROI Specialist
* Health Records Technician
* Jr. Clerk
* Mail Clerk
* Medical Transcriptionist
* Nutrition Technician
* O.R. Booking Clerk
* Patient Accounts Clerk
* Registration/Booking Clerk
* Secretary, Lev-3
* Secretary, Lev-4
* Sr. Clerk, Chart Completion
* Sr. Clerk, Chart Control
* Sr. Patient Accounts Clerk
* Switchboard Operator
* Unit Clerk

######   
ST JOE'S - POSITIONS OUT OF NEW BARGAINING UNIT

* Executive Assistant - VP
* Executive Assistant, President & CEO
* Secretary to the Board
* Administrative Assistant
* Administrative Assistant - Department
* Administrative Assistant - Program

######   
ST JOE'S POSITIONS IN DISPUTE

* Payroll Assistant
