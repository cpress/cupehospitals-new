+++
title = "Clerical at UHT vote overwhelmingly in favour of CUPE"
author = "CUPE Hospitals"
date = "2019-05-06T00:00:00-04:00"
description = "Clerical workers at Unity Health Toronto vote to join CUPE."
featured = "uploads/celebrates_stripes.png"
featuredpath = "/img/"
format = "Merger Update, Clerical"
tags = ["UHT Clerical", "Unity Health Toronto"]
type = "post"
categories = ["News"]

+++

## Congratulations!

348 CUPE members at St. Joseph's Health Centre Toronto will continue to be represented by CUPE. And, approximately 700 non-union clerical employees at Providence Healthcare Toronto and St. Michael's Hospital will now also be represented by CUPE.

#### Results

Regardless of the interference from the employer and SEIU to try and persuade workers to vote non-union, the vote results are:

- 533 in favour of CUPE representation
- 168 against


CUPE welcomes all the new members at Unity Health Toronto to CUPE. These new members will officially be represented by CUPE once a final decision comes from the Ontario Labour Relations Board which takes approximately 2 weeks.

Once this happens CUPE will be setting up a meeting for this new CUPE clerical group. Watch for details!
