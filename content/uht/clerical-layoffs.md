+++
author = "CUPE Hospitals"
date = "2019-05-02T13:00:00-04:00"
description = "Dozens of clerical staff at St. Mike’s, St. Joseph’s and Providence – Toronto’s most recent hospitals to undergo restructuring – have been laid off, many with two weeks’ notice, in contradiction to Premier Ford’s election promise that no one in the public sector would lose their jobs. The Canadian Union of Public Employees (CUPE) today called for the government to honour its commitment and to find a solution."
featured = "uploads/press-clerical-stmikes.jpg"
featuredpath = "/img/"
format = "Merger Update, Clerical"
tags = ["UHT Clerical", "Unity Health Toronto"]
title = "Ford's hospital restructuring results in hospital clerical staff being laid-off - with 2 weeks' notice"
type = "post"

+++


“At least 50 clerical staff at St. Mikes, St. Joe’s and Providence hospitals are losing their jobs – something this government promised would not happen,” says Sharon Richer, a hospital ward clerk and the secretary-treasurer of the Ontario Council of Hospital Unions (OCHU).
<center>

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/nEZVOat5Cc4" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


</center>

“More than 80% of hospital staff are women. When the PC changes to Ontario’s public health care system come in full force and “job losses hit under this restructuring, it is this female workforce that will be disproportionately and adversely affected. And in 2019, that should not be acceptable public policy, says Richer.

Recently, the government found $1.6 billion to ensure that staff reductions in education are handled by attrition, rather than layoff.

“The women at these hospitals who are being dumped after 14 years and more of service, with little notice, surely deserve equal treatment. We call on the government to step in with funding to rescind the layoffs and bring these women back to work while a permanent solution that honours the Premier’s pledge is found, says Richer.

While the layoffs at the three restructured hospitals, now called Unity Health Toronto, is causing great apprehension and fear as to whose job is on the line next, a recent poll of more than 1,300 Toronto residents shows the vast majority think restructuring and merging hospitals will cost more and lead to a deterioration of services.
 
Just one-in-ten of poll respondents think that merging agencies will improve overcrowding and hallway health care or improve access to health care in Toronto. Fewer than two-in-ten think the new super agency, that’s the centrepiece of the PC restructuring plan, will end up saving the province money.

Further, seven-in-ten respondents oppose the privatization or automation of hospital clerical support services, with the vast majority rejecting using an auto kiosk to check into a hospital, rather than a hospital worker.

OCHU is the hospital division of the Canadian Union of Public Employees (CUPE) which commissioned Environics to conduct a survey between April 22 and 24 to determine the attitudes of City of Toronto residents towards recently announced Ford government restructuring and changes to elements of Ontario’s health care system.

The poll is being released just days after nearly 10,000 people from across Ontario came to Queen’s Park to protest the PC health care restructuring plan that will facilitate huge segments of the public health care system being privatized and cut.

-30-


Article links:

- [Clerical job losses at 3 Toronto hospitals, including St. Mike's: union official | 680 News](https://www.680news.com/2019/05/02/st-mikes-hospital-layoffs-union/)

<iframe src="https://webapps.9c9media.com/vidi-player/1.5.6/share/iframe.html?currentId=1673590&config=cp24/share.json&kruxId=InJgKD0f&rsid=ctvgmtvecp24mobileweb,ctvgmtvecp24globalsuite&siteName=&cid=%5B%7B%22contentId%22%3A1673590%2C%22ad%22%3A%7B%22adsite%22%3A%22ctv.cp24%22%2C%22adzone%22%3A%22embed%22%7D%7D%5D" width="560" height="315"frameborder="0" allowfullscreen></iframe>
