+++
author = "CUPE Hospitals"
date = "2019-04-25T00:00:00-04:00"
description = "Answers to some of the frequently asked questions  over the last few days."
featured = "/img/uploads/document-magnifier.jpg"
format = "Merger Document, Clerical"
tags = ["UHT Clerical", "Unity Health Toronto"]
title = "CUPE Clerical FAQ - Seniority, Dues and Bumping"

+++
## CUPE Clerical FAQ – Seniority, Dues and Bumping

Below are answers to some of the questions we have been getting from Clerical employees over the last few days.

#### **If I’m currently non-union and we vote to join CUPE will I lose all my Service with the employer and go to the bottom of the CUPE Seniority list?**

**No.** Non-union will not go to the bottom of the CUPE seniority list, that is a rumour.

In the past at St. Mike’s when mail room employees were brought into the SEIU bargaining unit, SEIU did not acknowledge the service date of those workers and refused to give them seniority. Not only was this a different process, one that didn’t protect non-union workers’ seniority like the PSLRTA does, but SEIU is a different union than CUPE.

For non-union employees your time worked with the employer, which is currently your ‘service’, will be used to calculate your seniority. **Non-union employees must be given their time with the employer as seniority – that is the law under a PSLRTA merger.**

Read more about Seniority [here](https://cupehospitals.ca/facts/seniority-clerical/).

#### **How much are dues with CUPE?**

Union dues at CUPE start at 0.85% of your regular wages only. After that it's up to the local to determine what additional dues it will levy. On average, most locals choose 0.65% which brings the total to 1.5%, or $1.50, on every $100 of regular wages earned.

That's less than the cost of a cup of coffee at Tim Horton's - and it brings you an impressive range of services, as well as strong Collective Agreements.

Dues are tax-deductible meaning that they are automatically deducted from the amount of your income that is subject to income tax.

#### If I join CUPE will I get ‘bumped’ out by employees from another site?

There is some misinformation going around some of the hospital locations making people feel that if they choose to join CUPE that they will be bumped out of their jobs by workers who were already CUPE members. This is not what bumping is.

The CUPE collective agreement provides for ‘chain bumping’ **only in the event of a layoff**, which is the right to bump anyone with less seniority than you in any job that you can do in the bargaining unit, in any department, on any shift. And anyone that you bump can also bump anyone whose job she can do at any site, on any schedule, on any shift that she chooses. **This is superior to any other health care union’s language in Ontario**.

With CUPE’s language the union can propose alternatives to layoff or elimination of positions and take those alternatives to the CEO and to the board of directors. **In addition, early retirement and voluntary exit packages must be offered to the same number of people that the hospital would other-wise be laying off. All of this must happen before any individual can receive notice of layoff.**

Employers don’t want to layoff CUPE members because there is a domino effect coming as a result of **‘chain bumping’** because it is very disruptive and expensive for the employer.

For more information on Job Security and Chain Bumping click [here](https://cupehospitals.ca/facts/cupe-job-security-clerical/).

#### SEIU is telling Clerical to vote non-union so that clerical can join SEIU after the merger vote, is this a good idea?

**No.**  The merger vote is a choice between CUPE and Non-union.  If Clerical employees decide to vote non-union then after the vote there will be no protection for clerical employees and the restructuring, layoffs and terminations at St. Mike's will continue and likely spread to St. Joe's and Providence.

In order for clerical employees to join SEIU after the merger vote SEIU would have to have at least 40% of all Clerical employees at all three hospitals provide their personal information and sign SEIU membership cards.  This process can easily take a year or more and in that time the hospital will be able to restructure as it likes without any protection for clerical employees.  SEIU has tried several attempts to organize the clerical employees at St. Mike's and has never been successful.
