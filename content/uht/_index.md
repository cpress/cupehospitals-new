+++
title = "Unity Health Toronto (UHT)"
description = "Providence Healthcare Toronto, St. Joseph’s Health Centre, and St. Michael’s Hospital merged together to become Unity Health Toronto (UHT)"


+++

On August 1, 2017 Providence Healthcare, St. Joseph’s Health Centre and St. Michael’s Hospital merged together to become one new hospital, Unity Health Toronto (UHT).

<center>
<figure>
  <img src="/img/uploads/unity-health-toronto-header.jpg" width="75%" />
    <figcaption>
	<h4>The new logo of Unity Health Toronto and partner hospitals</h4>
	</figcaption>
</figure>
</center>
## Mergers


The merger of St. Joeseph's, St. Michael's, and Providence into UHT began changing how it provided services and this meant that work historically done by health care workers at the three different sites who are represented by various unions, started to change and merge.

["PSLRTA"](/facts/pslrta/) is the legislation (called the "Public Sector Labour Relations Transition Act") used during the merger of hospitals that sets out how process for determining what the new workforce structure will be. It is the process that determines what employee bargaining units will be established at the newly merger hospital as well as the process for determining which union, if any, will represent the workers.

## Bargaining Units at Unity Health Toronto

Among several bargaining units determined by the Labour Board, the Board issued a Decision that there should be one service bargaining unit and one clerical bargaining unit. 

1. [UHT clerical employees voted overwhelmingly to be represented by CUPE](/uht/uht-clerical-join-cupe/).
1. UHT service employees are represented by CUPE and SEIU.

## Votes

The make-up of the bargaining units means that there will be **two** votes held by the Labour Board. 

1. [UHT clerical employees voted overwhelmingly to be represented by CUPE](/uht/uht-clerical-join-cupe/)
2. The next vote will determine if service employees wish to be represented by CUPE or SEIU.

## CUPE's Central Agreement

CUPE has a collective agreement that has contract language that covers all CUPE members working as hospital employees.

[The CUPE Central Agreement can be downloaded from here.](/facts/cupe-central-agreement/)
