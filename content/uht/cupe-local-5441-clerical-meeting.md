+++
author = "CUPE Hospitals"
date = "2019-05-27T00:00:00-04:00"
description = "At this meeting you will meet your CUPE National Representative, Faiz Ahmed and find out what the next steps are now that all clerical at UHT are CUPE members. You will also be electing Stewards and Bargaining Committee representatives."
featured = "/img/uploads/document-magnifier.jpg"
featuredpath = ""
format = "Clerical"
tags = ["Unity Health Toronto", "UHT Clerical"]
title = "CUPE 5441 Clerical Meeting"

+++
### CUPE LOCAL 5441 CLERICAL UNIT MEETING - UHT

Please come out for the first CUPE Clerical Unit Meeting

**Tuesday, June 4th**  
5:30 p.m.

**SCADDING COURT COMMUNITY CENTRE**  
707 Dundas St W, Toronto

At this meeting you will meet your CUPE National Representative, Faiz Ahmed and find out what the next steps are now that all clerical at UHT are CUPE members.

You will also be electing Stewards and Bargaining Committee representatives.

A Steward training is set up for Saturday, June 15th.  If you are interested in attending the Steward training but are unable to attend the unit meeting on June 4th, please contact Faiz Ahmed at fahmed@cupe.ca.