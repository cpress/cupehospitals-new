+++
author = "CUPE Hospitals"
date = "2019-04-19T00:00:00-04:00"
description = "CUPE members are receiving misleading calls from SEIU"
featured = "uploads/member-update-small.png"
featuredpath = "img"
format = ""
tags = []
title = "Alert to all CUPE members!"

+++
**Alert for all CUPE members!**

CUPE has received reports that CUPE members are receiving calls from people that say they are calling from ‘your union’. It’s only if asked, which union, that the people say they are from SEIU. When our members call them out on being deceitful the SEIU representative hangs up. CUPE is not making calls to members.

**If you receive a call like this on your cell phone please try and record the call. If you are able to record the call please send to Deb Oldfield, CUPE National Representative at** [**doldfield@cupe.ca**](mailto:doldfield@cupe.ca) **so they can be forwarded to CUPE’s legal department.**

Also, please make your co-workers aware of this. It’s unfortunate that any union would stoop so low as to try and deceive another union’s members but not all unions operate with strong trade union principles, and certainly not all union’s are able to speak about their strength and successes as CUPE can.

Have a safe and happy Easter weekend!