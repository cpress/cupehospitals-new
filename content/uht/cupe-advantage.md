+++
title = "The CUPE Advantage for Clerical at UHT"
author = "CUPE Hospitals"
date = "2019-04-28T09:00:00-04:00"
description = "Union dues at CUPE are on average 1.5% or $1.50, on every $100 of regular wages earned. That's less than the cost of a cup of coffee, but you get so much more. Compare for yourself the difference between CUPE and Non-union and see how big the CUPE advantage is."
format = "Merger Update, Clerical"
featured = "/img/uploads/cupeadvantage.png"
featuredpath = "."
featuredalt = ""
tags = ["Unity Health Toronto","UHT Clerical"]
type = "post"

+++


# Job Security

## <img src="/img/uploads/checkmark.png" height="25pt"> CUPE Advantage

If you are deemed laid off for any reason the employer must offer early retirement packages and then early leaving packages to all employees in your job classification.  Only then if you are still surplus do you get your 5 months notice an you have the right to bump into another position at any location.  This applies to all full-time and part-time employees.  Your job may be gone, but you are not. (Article 9.08 -9.11, 10.03)

## <img src="/img/uploads/xmark.png" height="18pt"> Non-union

2 weeks notice of layoff and severance based on the Employment Standards Act. The employer may let you apply for other jobs but you must apply and interview for  the job. This is happening right now at St. Mike’s.  No protection during  restructuring or budget cuts. 

---
# Just Cause & Representation

## <img src="/img/uploads/checkmark.png" height="25pt"> CUPE Advantage

When you are a union member you can only be disciplined or terminated for just cause.  As a union member you are also entitled to have union representation at meetings with the employer. (Article 7.06)

## <img src="/img/uploads/xmark.png" height="18pt"> Non-union

There is no protection for non-union employees. Employees who are terminated must hire outside legal representation in order to fight a termination. For other forms of discipline there are no options for non-union employees. 


___
# Benefits for Part-time
## <img src="/img/uploads/checkmark.png" height="25pt"> CUPE Advantage

Part-time employees receive 14% in lieu of benefits on top of their regular wage rate.  Depending on hours worked this is several thousand dollars more in your pocket which can be used to buy benefits through the employer.  (Article 18.04)
 
## <img src="/img/uploads/xmark.png" height="18pt"> Non-union

$1000 per year flex benefits if you qualify. Must be used each year or lost. 

---

# Early Retiree Benefits
## <img src="/img/uploads/checkmark.png" height="25pt"> CUPE Advantage

The hospital will provide equivalent benefit coverage to early retirees to age 65. (Article 18.01e)

## <img src="/img/uploads/xmark.png" height="18pt"> Non-union

N/A

---

# Job Postings & Promotions

## <img src="/img/uploads/checkmark.png" height="25pt"> CUPE Advantage 

The hospital must post vacancies that occur within 30 days.  If you are the senior applicant and meet the normal requirements of the job, you get the job. (Article 9.05)

## <img src="/img/uploads/xmark.png" height="18pt"> Non-union

No requirement for jobs to be posted. Employer can pick who ever they choose without ever having even posted the job.

---
# Vacation


## <img src="/img/uploads/checkmark.png" height="25pt"> CUPE Advantage

No cap on number of weeks that can be taken and vacation scheduling is based on seniority. (Article 17)

## <img src="/img/uploads/xmark.png" height="18pt"> Non-union

Not able to take more than 2 weeks vacation at one time and vacation approval is at the employer’s discretion.

---

To download the CUPE Central Collective Agreement go to [cupehospitals.ca/uht/](https://cupehospitals.ca/uht/).

---
