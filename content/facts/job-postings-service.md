+++
author = "CUPE Hospitals"
date = "2018-12-09T14:36:55-05:00"
description = "CUPE's superior seniority based job posting language prevents the employer from cherry-picking employees."
format = "Hospitals, Mergers"
title = "CUPE - Job Postings | Service"
type = "itemized"

+++
## **THE CUPE ADVANTAGE - SUPERIOR JOB POSTING LANGUAGE**

CUPE’s job posting and promotion language is better than any other health care union **and is the only job promotion language that is seniority based.** This means if you are the senior applicant and meet the normal requirements of the job, you get the job. This language ensures that our members are not disadvantaged because their interview skills are rusty and it prevents the employer from cherry picking applicants.

In addition, CUPE’s posting language requires the hospital to post all permanent vacancies within 30 calendar days of the position becoming vacant.

Compare For Yourself:

#### **CUPE’s job posting language from CUPE’s Central Agreement**

> **9.05 d)**  _In matters of promotion and staff transfer appointment shall be made of the **senior applicant able to meet the normal requirements of the job.** Successful employees need not be considered for other vacancies within a six (6) month period unless an opportunity arises which allows the employee to change his or her permanent status._

With CUPE’s job posting language its simple, if the senior applicant is able to meet the normal requirements of the job they get the job.  The normal requirements of the job are the requirements shown on the job posting.

**This eliminates the employer’s ability to manipulate the process and pick their favourites.**

#### **SEIU’s job posting language Article 11.02 is what’s known as a Competition Clause:**

> _11.02 Employees shall be selected for positions on the basis of their ability, experience, qualifications and skills. **Where these factors are relatively equal among the employees considered, seniority shall govern**, providing the successful applicant, if any, is qualified to perform the available work._

With SEIU’s job posting language seniority is only a factor if the applicant’s ability, experience, qualifications and skills are relatively equal.  **Who gets to make the decision about whether these factors are equal?  The employer**.

**This means the employer can manipulate the outcome of a job posting and use it to pick their favourites** rather than the senior, qualified applicant.

The choice is clear, CUPE has the best language of any health care union in the Province.