+++
author = "CUPE Hospitals"
date = "2018-09-06T18:25:23+00:00"
description = "When at least 40% of employees before the representation vote are non-union, then non-union is on the ballot.  For clerical at Providence, St. Joseph’s and St. Mike’s your choice will be between CUPE and non-union.  If a simple majority of those who vote, vote to either stay with, or join CUPE, then CUPE will be the union for all clerical employees at all three hospitals."
format = "Mergers, Unionization"
title = "Hospital Mergers | Clerical"
type = "itemized"

+++
## Mergers:  the Composite Collective Agreement
When at least 40% of employees before the representation vote are non-union, then non-union is on the ballot.  For clerical at Providence, St. Joseph’s and St. Mike’s your choice will be between CUPE and non-union.  If a simple majority of those who vote, vote to either stay with, or join CUPE, then CUPE will be the union for all clerical employees at all three hospitals.

## Non-Union Employees

If after the vote CUPE is the Union representing Clerical with the new employer, the previously non-union employees at Providence and St. Mike’s will have their working conditions frozen until a new collective agreement has been negotiated.  

> Your wages, benefits and working conditions can not be changed.  That’s the law.

Also after the vote all non-union employees will be covered by 4 key areas of the CUPE Collective
Agreement:

- seniority,  
- job posting,  
- layoff, recall & bumping language, and  
- grievance process  

## CUPE Members

If after the vote CUPE is the Union representing Clerical employees with the new employer, CUPE members will continue to be covered by their Collective Agreement.  Your wages, benefits and working conditions cannot be changed.  That’s the law.

### The Composite Collective Agreement
The wages, benefits and working conditions of CUPE members and non-union employees continue and all employees will be covered by CUPE’s seniority, job posting, layoff, recall & bumping language, and grievance process.  This is called the ‘composite’ collective agreement, and it remains in effect until a new collective agreement is negotiated between the employer and the successful union.  

Because CUPE bargains a Central Agreement with the OHA the new Collective Agreement will be comprised of CUPE’s Central Agreement and the ‘harmonized’ Local Issues Agreement which takes the best rates of pay and working conditions and harmonizes them to the highest and best so that no one loses as a result of the merger.  CUPE has already negotiated a Central Agreement until 2021 which includes wage increases in each year of the Agreement that would apply to everyone after the vote. 

Often employers see bargaining after the merger as a chance to gain concessions. 

CUPE wants to protect hard won gains that workers and unions have achieved over decades. 