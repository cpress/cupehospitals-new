+++
draft = false
featured = "/img/2017/10/rpn-info-cut.png"
date = "2017-10-20"
title = "UHT vote email"
description = "The hospital merger vote will likely be online and through the employer email."
tags = ["UHT Clerical", "Unity Health Toronto"] 
type = "itemized"
format = "UHT Vote"
author = "CUPE Hosptials"

+++


The hospital merger vote will likely be online and through the employer's email system.

# To vote 

1. Log-in to your employer email system:

	- St. Mike’s https://owa.smh.ca
	- St. Joe’s https://webmail.stjoestoronto.ca
	- Providence https://portal.providence.on.ca/  
	username: prc/(username)
2. Look for the vote email message and get your **PIN**.
3. Follow the instructions in the email.

CUPE will be sending notifications to you to remind you to vote every day of the vote. We will also be calling those who have not yet voted to see if you need help to vote or have any questions.

> The vote is conducted by the Ontario Government’s Labour Board and is completely confidential. No one will know how you voted.

