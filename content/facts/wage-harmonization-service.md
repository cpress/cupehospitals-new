+++
author = "CUPE Hospitals"
date = "2018-09-05T12:30:09+00:00"
description = "When harmonizing wages, CUPE identifies similar job classifications from all hospital locations.  Once that is done CUPE takes the highest rate of pay for those similar jobs and negotiates that rate for that job at all locations."
format = "Mergers"
title = "Wage Harmonization | Service"
type = "itemized"

+++
## Negotiating a new collective agreement

With the Merger under the Public Sector Labour Relations Transition Act (PSLRTA) you get the successful union’s seniority, lay-off, recall and bumping, job posting and grievance provisions. Then the rest of the collective agreement must be negotiated and with CUPE that means having the CUPE Central Agreement, negotiated with the Ontario Hospital Association (OHA).

> After the vote all employees, CUPE and previous SEIU members, would have the protection of being covered by CUPE’s Central Agreement until 2021.

This is great protection with looming funding cuts to health care.

Then the Local Agreement is negotiated with the goal to ensuring that the ‘best of’ language from all previous contracts from both unions is included in the new Local Agreement.

CUPE has a great track record of negotiating best language from each Union’s collective agreement into the new collective agreement as well has harmonizing wages to the highest rates.

Often after a merger, employers see it as an opportunity to strip hard won benefits from predecessor collective agreements. This means that after the vote it is essential that we focus on bargaining a strong new agreement.

For CUPE that means ensuring that there is representation at the bargaining table from all previous unions because they know their collective agreement and we want to ensure that we ‘harmonize’ up to the best language in all areas of all the previous collective agreements. No one should have to lose rights and benefits achieved over decades of bargaining. With CUPE’s resources and expertise, you have Canada’s largest union on your side to secure the best new contract that works for you.

## Harmonization

Negotiating the local issues collective agreement with CUPE means negotiating to harmonize local issues to the highest. Things like benefits, vacation, shift premiums and wages all must be ‘harmonized’.

When harmonizing wages, CUPE identifies similar job classifications from all hospital locations.  Once that is done CUPE takes the highest rate of pay for those similar jobs and negotiates that rate for that job at all locations.

If the employer won’t agree to the new rate, CUPE will take it to arbitration.

> CUPE has never lost an arbitration on bringing all wage rates to the highest level following a PSLRTA vote.  There is no holding back of wage increases, called ‘red circling’, for those earning a higher rate of pay until the lower rates catch up.

There is **no red circling of wages** in the PSLRTA process.

This is why it’s so important to choose the union that will be best able to achieve the best new collective agreement and CUPE members know that CUPE negotiates to maintain superior protections and improve working conditions in the hospital sector.
Without doubt CUPE is the union best suited to represent clerical workers in health care and provide the strength and protection that is essential during a merger and provincial funding cuts.