+++
author = "CUPE Hospitals"
date = "2018-09-06T18:11:02+00:00"
description = "The Canadian Union of Public Employees (CUPE) represents more than 80,000 CUPE members working in hospitals, long-term care, community care and paramedicine, 40,000 of those members work at 120 sites of more than 60 hospitals."
format = "Mergers"
title = "CUPE - Who we are | Service"
type = "itemized"

+++
## The Canadian Union of Public Employees is Canada’s Health Care Union

The Canadian Union of Public Employees (CUPE) represents more than 80,000 CUPE members working in hospitals, long-term care, community care and paramedicine, 40,000 of those members work at 120 sites of more than 60 hospitals.

CUPE represents registered practical nurses, personal support workers, porters, dietary, cleaning, reprocessing, trades, clerical and administrative staff, paramedics and paramedical staff.

## A Democratic Union

CUPE is a democratic union committed to local autonomy. CUPE locals make decisions about bargaining priorities, grievances and arbitrations and even dues. In other words, as a member, your vote has real strength.

CUPE Locals are based on workplaces.  This means your elected officers such as your Local President, Vice President, Secretary Treasurer and Recording Secretary are all elected from members in the workplace, by members in the workplace.

## A Campaigning Union

In addition to securing improvements to working conditions at the bargaining table, CUPE also mo- bilizes public pressure on government decision- makers to enhance and protect public health care.  In Ontario, CUPE members run effective advocacy campaigns, on a number of health care issues, including: increased staffing in the public health care system; an end to health care cuts and privatization; legislative protection against violence in the workplace; and a reduction in deaths due to hospital acquired infections and adequate funding for hospital care.

## Ontario Council of Hospital Unions (OCHU)

Founded in 1982, the 40 000 member Ontario Council of Hospital Unions (OCHU) is the hospital division of CUPE in Ontario.

<img src="/img/ochu-logo.jpeg" align="right" width="35%">

OCHU/CUPE bargains a provincial collective agreement with the Ontario Hospital Association (OHA) and lays that pattern down across the hospital sector and for those long-term care facilities that have a relationship with a hospital. Because we have the strongest collective agreement in the province – and we enforce it – OCHU/CUPE hospital locals have the lowest rate of layoff in Ontario.

OCHU/CUPE carries out advocacy on behalf of our members and on behalf of hospital patients and long-term care residents across Ontario.

OCHU/CUPE works closely with the Ontario Health Coalition whenever community health services are threatened with cuts or privatization.
CUPE’s Strength Matters for You

* 650,000 members across the country.
* Ontario's largest union with 250,000 members.
* Canada's largest health care union with 80,000 health sector members.
* $16M Defence Fund for cost-share campaigns between bargaining.
* Canada's largest in-house team of legal, research and communications representatives.