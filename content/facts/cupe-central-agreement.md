+++
author = "CUPE Hospitals"
date = "2018-09-06T18:16:12+00:00"
description = "CUPE negotiates a collective agreement that covers most CUPE workers in the hospital sector in Ontario."
format = "CUPE Advantage"
title = "CUPE's Central Agreement"
type = "itemized"
tags = ["UHT Clerical","Unity Health Toronto"]

+++
## The Canadian Union of Public Employees is Canada’s Health Care Union
Like our name says, CUPE is a Canadian Union.  CUPE represents more than 80,000 CUPE members working in hospitals, long-term care, community care and paramedicine, 40,000 of those members work at 120 sites of more than 60 hospitals.

CUPE represents registered practical nurses, personal support workers, porters, dietary, cleaning, reprocessing, trades, clerical and administrative staff, paramedics and paramedical staff.

OCHU/CUPE bargains a provincial collective agreement with the Ontario Hospital Association (OHA) and lays that pattern down across the hospital sector and for those long-term care facilities that have a relationship with a hospital. Because we have the strongest collective agreement in the province – and we enforce it – OCHU/CUPE hospital locals have the lowest rate of layoff in Ontario.

[![](/img/uploads/cupecentral.png)](/img/uploads/cupecentral.pdf)

[*Download CUPE's Central Agreement here in PDF format.*](/img/uploads/cupecentral.pdf)


OCHU/CUPE also carries out advocacy on behalf of our members and on behalf of hospital patients and long-term care residents across Ontario. OCHU/CUPE works closely with the Ontario Health Coalition whenever community health services are threatened with cuts or privatization.
CUPE’s Matters for You

