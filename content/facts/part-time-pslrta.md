+++
author = "CUPE Hospitals"
date = "2019-01-01"
description = ""
draft = false
format = "mergers"
tags = ["UHT Clerical", "Unity Health Toronto"]
title = "UHT Part-time Employees under PLSRTA"
type = "itemized"

+++


Under PSLRTA the Ontario Labour Relations Board (OLRB) can determine what the 'new' bargaining units will look like. For Unity Health Toronto, the OLRB has ordered that there be one combined full-time and part-time Service unit and one full-time and part-time Clerical unit.

This means that after the merger vote when all employees are covered by the CUPE Central Agreement, all employees will be entitled to the rights and benefits set out in that Collective Agreement, and that includes part-time employees in both the Service and Clerical units.

## Seniority

PSLRTA requires that after a Merger Vote the successful Union’s
seniority provisions apply to all employees and that includes the
definition of seniority, the lay-off/recall/ bumping language and the
job posting process.

PSLRTA also ensures that as non-union employees your service will be
converted to seniority and “dovetailed” with the unionized employees
with 1725 hours equal to one year for part- time employees. CUPE’s job
security and seniority provisions are the strongest of any hospital
union in Ontario.

In addition, Seniority for all employees transfers over when going
from part-time to full-time or from full-time to part-time. There is
absolutely no risk of any employee, union or non-union, losing their
hours or seniority, it is protected by law.

## Job Posting

With a CUPE collective agreement part-time employees receive the same
rights and benefits as full-time employees such as job security for
scheduled shifts, maternity leave, bereavement leave, safety footwear,
vacation, etc. As a result of CUPE’s seniority based job posting
language, once the CUPE Central Agreement applies, part- time
employees will be treated equally in the job promotion process. If a
part-time employee can do the job and they have the seniority, they
get the job.

## Percent In Lieu of Benefits

As a part-time employee with CUPE you receive 14% in lieu of benefits
which results in our members earnings several thousand dollars more
per year. Part-time employees can purchase benefits from the
employer's benefit plans. See example of the CUPE advantage for
part-time employees on the back.

## Vacation

With CUPE part-time employees are entitled to vacation as follows:

- Less than 3,450 hours: 4% vacation pay Completed 3,450, but less than
8,625: 6%
- Completed 8,625, but less than 20,700: 8%
- Completed 20,700, but less than 34,500: 10%
- Completed 34,500 but less than 48,4300: 12%
- Completed 48,300: 14%

## Pension

Also, with the CUPE Central Agreement part-time employees can join the
pension plan without a reduction to their percent in lieu of benefits.
With CUPE, part-time members get 14% in lieu with no reduction.
