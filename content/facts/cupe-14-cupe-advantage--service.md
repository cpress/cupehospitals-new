+++
author = "CUPE Hospitals"
date = "2018-12-09T15:39:10-05:00"
description = "Take a look at the difference 14% in lieu of benefits makes for CUPE part-time members."
format = "Hospitals, Mergers"
title = "CUPE - 14% CUPE Advantage | Service"
type = "itemized"

+++
## Part time workers, benefits and the percent in lieu - The CUPE Advantage

If you compare three different workers, each earning the same hourly rate, in this example $25.00 per hour, you will see that earning 14% in lieu of benefits puts much more money in your pocket and allows you to purchase more benefits if that's what you need.

#### CUPE part-time member

![](/img/uploads/graphic Sandy pic.png)

Sue is a part time CUPE member at St Joe’s and works 24 hours per week. Sue earns **$25.00** per hour or **$31,200 per year**.

Because Sue is a CUPE member she also earns **an additional 14 per cent in lieu of benefits**, which is not reduced even though Sue participates in the pension plan (HOOPP).

**This now means that Sue earns an additional $4,368 per year.**

Sue is now earning **$35,568** not including any per cent in lieu of vacation.

If Sue wants to buy benefits with the $4,368 she can.  If she has benefit coverage at another job or with a partner, she can use it on anything she likes.

**For Sue the CUPE advantage is $3,268 more dollars in her pocket.**

#### SEIU part-time member

![](/img/uploads/graphic Dave pic.png)

Dave is a part time SEIU member at St Mike’s and works 24 hours per week.  Dave earns **$25.00** per hour or **$31 200 per year.**

Because Dave is an SEIU member he gets a **credit of $1,100 that he can use to buy from the employer’s flex benefit plan**.  

Dave doesn’t get a per cent in lieu of benefits and **if Dave doesn’t use the $1,100 worth of benefits he loses it** because it can’t be cashed out or carried over to another year.

**Dave earns $31,200 per year plus $1,100 worth of benefits ($32,300)**.

#### SEIU part-time (casual) member

![](/img/uploads/graphic Sue pic.png)

Sandy is a part-time casual SEIU member at St Mike’s and usually works 12 hours per month.  Sandy earns **$25.00** per hour. Sandy earns **$3,600 per year**.

Because Sandy is an SEIU member and doesn’t qualify for the employer’s flex benefit plan **she gets 8% in lieu of benefits**.

Sandy is now earning **$3,888 per year.**

**If Sandy was a CUPE member she would get 14% in lieu of benefits and earn $4,104 per year.**

For Sandy CUPE advantage is **$216**.