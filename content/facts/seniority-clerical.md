+++
author = "CUPE Hospitals"
date = "2018-08-28T14:17:16+00:00"
description = "For non-union employees your time worked with the employer, which is currently your ‘service’, will be used to calculate your seniority."
format = "Unionization | Clerical"
title = "Seniority | Clerical"
type = "itemized"
tags = ["UHT Clerical","Unity Health Toronto"]

+++
## Your seniority is protected

For non-union employees your time worked with the employer, which is currently your ‘service’, will be used to calculate your seniority.

Seniority of all employees, including those that were non-union, will be based on the common definition of seniority of the successful union and applied equally to everyone in the bargaining unit.

CUPE's seniority is calculated from date of last hire for full time and 1725 hours equals one year for part time employees.  Seniority for all employees transfers over when going from part time to full time or from full time to part time.

As a union member seniority is used for
applying for job postings, vacation scheduling and in the case of bumping, layoffs and recall.


#### You cannot lose your seniority, that’s the law. 

The Public Sector Labour Relations Act (PSLRTA) which is the piece of legislation that governs Hospital mergers, ensures you will not lose your seniority and requires that seniority from the pre-merger bargaining units, along with non-union service, be “dove-tailed”.

After the vote the employer will calculate seniority for previously non-union employees based on CUPE’s definition of seniority and then merge those seniority dates together with the CUPE seniority dates.  

Once this has been completed the employer will be required to post the list for a period of at least 30 days so that employees have an opportunity to ensure their seniority date appears correct on the new list.  

If there are any issues with seniority dates the employee will let the employer know so that the employer can make necessary changes.
