+++
author = "CUPE Hospitals"
date = "2018-09-05T12:25:44+00:00"
description = "CUPE has never lost an arbitration on bringing all wage rates to the highest level following a PSLRTA vote. There is no holding back of wage increases, sometimes called ‘red circling’, for those earning the higher rate of pay until the lower rates catch up."
format = "Unionization"
title = "Wage Harmonization | Clerical"
type = "itemized"
tags = ["UHT Clerical","Unity Health Toronto"]

+++
## Negotiating a New Collective Agreement
With the Merger under the Public Sector Labour Relations Transition Act (PSLRTA) you get the successful union’s seniority, lay-off, recall and bumping, job posting and grievance provisions. Then the rest of the collective agreement must be negotiated and with CUPE that means having the CUPE Central Agreement, negotiated with the Ontario Hospital Association (OHA).

> After the vote all employees, CUPE and previous non-union members, would have the protection of being covered by CUPE’s Central Agreement until 2021.  

This is great protection with looming funding cuts to health care.

Then the Local Agreement is negotiated with the goal to ensuring that the ‘best of’ wages, benefits and working conditions from all previous employee groups are included in the new Local Agreement.

CUPE has a great track record of negotiating the ‘best of’ from each employee group and collective agreement into the new collective agreement as well has harmonizing wages to the highest rates. 

Often after a merger, employers see it as an opportunity to strip hard won working conditions and benefits from workers and predecessor collective agreements. This means that after the vote it is essential that we focus on bargaining a strong new agreement.

For CUPE that means ensuring that there is representation at the bargaining table from the previous non-union clerical group because they know their working conditions and benefits.  We want to ensure that we ‘harmonize’ up to the best language in all areas. No one should have to lose rights and benefits achieved over decades of employment and bargaining. 

With CUPE’s resources and expertise, you have Canada’s largest union on your side to secure the best new contract that works for you.

## Harmonization

Negotiating the local issues collective agreement with CUPE means negotiating to harmonize local issues to the highest. Things like benefits, vacation, shift premiums and wages all must be ‘harmonized’.

When harmonizing wages, CUPE identifies similar job classifications from all hospital locations.  Once that is done CUPE takes the highest rate of pay for those similar jobs and negotiates that rate for that job at all locations.  If the employer won’t agree to the new rate, CUPE will take it to arbitration.  

> CUPE has never lost an arbitration on bringing all wage rates to the highest level following a PSLRTA vote. 

There is no holding back of wage increases, called ‘red circling’, for those earning the higher rate of pay until the lower rates catch up. 

> There is **no red circling of wages** in the PSLRTA process.

The only way to ensure that wages are raised up, not lowered is for CUPE to represent all clerical employees after the vote.  

Without a union there is absolutely no protection and when the hospital looks for ‘efficiencies’ they can easily reduce the job rate of clerical employees or even lay off those that are earning the higher rate.  For employees who feel the hospital wouldn’t do something like that to them, also understand that changes at the management and supervisory level are also happening and the people protecting you now may not be there in the future.

Without doubt CUPE is the union best suited to represent clerical workers in health care and provide the strength and protection that is essential during a merger and provincial funding cuts.
