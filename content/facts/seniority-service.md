+++
author = "CUPE Hospitals"
date = "2018-08-28T14:20:27+00:00"
description = "You cannot lose your seniority, that’s the law. The Public Sector Labour Relations Act (PSLRTA) is the piece of legislation that governs Hospital mergers.  PSLRTA ensures you will not lose your seniority and requires that seniority of all employees from the pre-merger unions be “dove-tailed”."
format = "Mergers | Service"
title = "Seniority | Service"
type = "itemized"

+++
## Your Seniority is Protected

You cannot lose your seniority, that’s the law. The Public Sector Labour Relations Act (PSLRTA) is the piece of legislation that governs Hospital mergers.  PSLRTA ensures you will not lose your seniority and requires that seniority of all employees from the pre-merger unions be “dove-tailed”.  

The Ontario Labour Relations Board (OLRB) determined that there will be one combined bargaining unit for all full time, part time and casual service employees with the newly merged hospital.

This means the seniority of all employees will be based on the common definition of seniority of the successful union and applied equally to everyone in the bargaining unit. CUPE's seniority is calculated from date of last hire for full time and 1725 hours equals one year for part time employees. Seniority for all employees transfers over when going from part time to full time or from full time to part time.

After the vote the employer will merge the seniority lists so that there will be one seniority list and they will be required to post the list for a period of at least 30 days so that employees have an opportunity to ensure their seniority date appears correct on the new list.  

If there are any issues with seniority dates the employee will let the employer know so that the necessary corrections can be made.