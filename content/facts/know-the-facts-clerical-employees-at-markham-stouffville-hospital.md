+++
author = "CUPE Hospitals"
date = "2019-01-20T18:52:37-05:00"
description = "MSH Clerical Employees - Know the Facts"
format = "Job Security"
title = "Know the Facts - Clerical Employees at Markham Stouffville Hospital"
type = "itemized"

+++
The Hospital recently sent out a letter to all clerical employees trying to persuade you that joining a union may not be in your best interest.  Typically, the only time the Hospital worries about its employees’ financial circumstances and how employees choose to spend their money is when employees choose to join a union and pay union dues.  

Let’s be clear, the Hospital is not concerned about your financial wealth, and in fact, it likely isn’t even about a fear of having to pay higher wages.  The Hospital does not want you and your colleagues to join CUPE because when you join CUPE you have the protection of a strong Collective Agreement and the Hospital loses the absolute control they enjoy right now.

CUPE has the strongest Job Security protection of any Health Care Union in Ontario.  This is the protection that hospital workers need now more than ever because with this Conservative Provincial Government, hospitals across the province are looking for ways to cut costs by cutting jobs, especially clerical positions.  In this past week along there have been reports by several news outlets that the Provincial Government is looking at reducing or eliminating the LIHNs and is looking at ways to reduce ‘hall way’ medicine by privatizing health care. 

Take a look at just some of the benefits of joining CUPE and having a Collective Agreement. 

**Wages:**  The Hospital recently sent out a letter comparing the wages of clerical employees to Uxbridge Cottage Hospital.  
**With a CUPE Collective Agreement you negotiate for your own group.  Wage rates are negotiated at each hospital by each bargaining unit.  Your wages will not go down and the hospital making any statement that implies that this would happen is a scare tactic on their part.**

**Dues:**  The Hospital recently advised you that you would have to pay dues in the amount of 2% because that is what the Local at the hospital pays now.    
**CUPE has 2 locals at the Hospital.  There is a CUPE local that represents service employees at the Markham site and also a CUPE local that represents office employees at Uxbridge as well as office and service employees at Lakeridge Health.  As a newly certified CUPE bargaining unit you and your colleagues will vote to decide on whether you want to join one of the existing locals or become your own local.  As your own local you would be able to set your own dues rate and the average CUPE dues rate is 1.5% of regular gross earning.  Dues are tax deductible and appear on your T4 every year.**  

**Part-time employees:**  The hospital reduces the percent in lieu of benefits for part-time employees when they participate in the HOOPP pension.   
**With a CUPE Collective Agreement there is no reduction of the percent in lieu when part-time employees participate in HOOPP.**

**Volunteers:**  For various types of positions hospitals have been replacing non-union clerical positions with volunteers.  Just like Markham Stouffville Hospital did when they fired the employee in the Occupational Health Secretary position and replaced her with a volunteer.  
**With a CUPE Collective Agreement volunteers & co-op students can’t be used in place of CUPE members.**

**Technology:**  Health Records positions are being replaced by electronic and voice applications accessed by doctors and nurses using tablets.  Patient Registration positions are being replaced by Patient Registration Kiosks.    
**With a CUPE Collective Agreement CUPE members can’t be forced out the door.  With CUPE’s job security language your seniority provides you the option of taking a retirement package or bumping into another position and the hospital must first provide each employee with 5 months’ notice so there is lots of time to make the decision that is right for you.**

**Contracting Out:**  Transcription positions are being contracted out to outside companies.  
**With a CUPE Collective Agreement the hospital can not contract out work if it results in the lay off of a CUPE member.  A lay off includes any reduction of hours.**

**Representation:**  Markham Stouffville Hospital has terminated long term employees after they have spent their career dedicated to the hospital.  
**With a CUPE Collective Agreement you have representation at meetings with the employer and the employer can only discipline a CUPE member for ‘just cause’.**

**Job Postings:**  Currently the hospital can hire whoever they want for a position.  They don’t even need to post the job so that employees can apply.  If the hospital chooses to hire outside the hospital for a vacant position, they can, even though many full-time and part-time employees are qualified for the job.  
**With a CUPE Collective Agreement the hospital is required to post vacancies.  They must hire within the hospital, including part-time employees, provided employees meet the normal requirements of the job which must be indicated on the job posting.**

**Leaving a Union:**  The Hospital claimed in their letter than once you join a union its very difficult to leave.  In fact, the process is pretty much the same.  Just as it requires at least 40% of employees to sign up with the union, it takes 40% of employees to sign up to leave the union.  
**CUPE strives to provide great support and resources and negotiates strong Collective Agreements so that members never feel like they want to leave the union.** 

 