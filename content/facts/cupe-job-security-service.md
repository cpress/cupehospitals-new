+++
author = "CUPE Hospitals"
date = "2018-08-28T12:57:13+00:00"
description = "CUPE has superior job security protections. The CUPE collective agreement provides for ‘chain bumping’ in the event of a layoff, which is the right to bump anyone with less seniority than you in any job that you can do in the bargaining unit, in any department, on any shift. And anyone that you bump can also bump anyone whose job she can do at any site, on any schedule, on any shift that she chooses.  This is superior to any other health care union’s language in Ontario. "
format = "Mergers | Service"
title = "CUPE & Job Security | Service"
type = "itemized"

+++
# Superior Job Security Language

## 5 Months Notice of Layoff
Under the CUPE Central collective agreement, the employer must give the union 5 months' written notice of any pending layoffs or elimination of positions.  This notice triggers the Redeployment Committee, which is entitled to ask for and to review all pertinent staffing and budget information related to the layoffs or the elimination of positions.

## CUPE’s Early Retirement Packages and Exit Packages
With CUPE’s language the union can propose alternatives to layoff or elimination of positions and take those alternatives to the CEO and to the board of directors. In addition, early retirement and voluntary exit packages must be offered to the same number of people that the hospital would other-wise be laying off. All of this must happen before any individual can receive notice of layoff. Any individual CUPE members must each be given 5 months' notice of layoff including those who have been bumped.

With SEIU the employer first gives the employee their layoff notice.  Then the employer will ‘reassign’ the laid off employee to a vacant position if one is available.  If there is no available position the employer polls employees and offers only ‘exit’ packages.  If an employee takes an exit package, then the laid off employee is ‘reassigned’ to this position.  If no employee takes an exit package, then the laid off employee can bump.

## CUPE’s Chain Bumping
Employers don’t want to layoff CUPE members because there is a domino effect coming as a result of ‘chain bumping’. The CUPE collective agreement provides for ‘chain bumping’ in the event of a layoff, which is the right to bump anyone with less seniority than you in any job that you can do in the bargaining unit, in any department, on any shift. And anyone that you bump can also bump anyone whose job she can do at any site, on any schedule, on any shift that she chooses.  This is superior to any other health care union’s language in Ontario.    

With SEIU’s language if no one takes an exit package then the laid off employee can bump the ‘most’ junior employee in the ‘same position’ with the ‘same description’.  This means that as a long-term employee you could be reassigned to or bump into a position at one of the other hospital locations on a completely different shift.  In addition, the SEIU language only allows for 2 bumps and then unfortunately the 3rd person bumped is out the door.

## CUPE’s Training Requirement
The Redeployment Committee identifies the training needs of workers who are or who would otherwise be laid off and facilitates the training.  The Hospital is required to award vacant positions to workers who would otherwise be laid off if, with six months retraining, they become able to meet the normal requirements of the job. Because of our strong bumping and layoff language, CUPE has the lowest rate of layoff and job cuts in the healthcare sector in Ontario.

## Superior Job Posting Language
CUPE’s promotion language is better than the other union’s and is the only job promotion language that is seniority based. This means if you are the senior applicant and meet the normal requirements of the job, you get the job. This language ensures that our members are not disadvantaged because their interview skills are rusty, and it prevents the employer from cherry picking applicants.

SEIU’s promotion language is what is called a ‘competition’ process.  This means that seniority is only used if the employer feels that employees applying for a job have the same ability, experience, qualifications and skills.  This allows the employer to manipulate the promotion process.


## Superior No Contracting Out Language
CUPE’s Collective Agreement does not allow the Hospital to contract out any work usually performed by members of the bargaining unit if, as a result of such contracting out, a layoff of any employees other than casual part-time employees results from such contracting out.