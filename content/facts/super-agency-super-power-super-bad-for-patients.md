+++
author = "CUPE Hospitals"
date = "2019-02-13T00:00:00-05:00"
description = "The Ontario PC government is proposing creating a centralized super agency which will receive all the funding and, ultimately, make all the key decisions about services in hospitals, long-term care (LTC), cancer care, Local Health Integration Networks (LHINs), community health, addictions, palliative care, and others. All of this is happening without any consultation with the public or with the health care workforce."
format = "Privatization"
title = "Super Agency, super power, super bad for patients"
type = "itemized"
tags = ["testings"]

+++
The super agency will have the power to, among other things:

* Consolidate hospital services from 150 hospitals down to 30-50 mega multi-sector health care organizations, 
* Absorb functions of the Local Health Integration Networks (LHINs),
* Roll in cancer care services,
* Oversee emergency health care services (e.g. ambulance services),
* Make key decisions about health care infrastructure and spending.

## Integrated Care Groups

The super agency will designate thirty to fifty “integrated care delivery systems” and may be called My Care Groups.

These new Integrated Care Groups will:

1. Unite hospitals, LTC, palliative care, primary care, home care, mental health and addiction services, and other services that may be added later.
2. Receive one single stream of funding via an accountability agreement with the agency. The idea is to encourage these integrated groups to move work to the lowest cost provider.

## Mass privatization, restructuring, and union representation votes under the Super Agency model

The health minister may require health service providers, like hospitals and long-term care homes, to use the procurement or supply chain services provided by the super agency.

This would include the power to order the privatization (contracting out) of services currently provided by CUPE hospital and health care workers.

The super agency could displace existing hospital, LTC, LHIN, and other supply chain and procurement providers.

* This threatens existing contracts with small- and medium-sized businesses that are unable to provide services at the scale demanded by the provincial super agency.
* It also threatens existing CUPE bargaining units involved in procurement and supply chain management (laundry locals, for example).

## Extracting profit from health care

> This reform is focused on reducing services in hospitals and long-term care and increasing profits of private corporations employing low- waged workers.

The approach the Ford PCs are taking is not unlike the last time a PC government closed hospitals and restructured the health system in the late 1990s. Today, we know that patients and front-line health care staff were those hurt most by the previous PC health system restructuring, and Ontario’s Auditor General found that it cost $3.2 billion more than it saved.
