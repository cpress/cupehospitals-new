+++
author = "CUPE Hospitals"
date = "2018-09-06T18:27:45+00:00"
description = "When employees of the newly merged hospital belong to different unions the Ontario Labour Relations Board (OLRB) will hold a vote to see which union the employees want to have represent them with the new employer."
format = "Mergers"
title = "Hospital Mergers | Service"
type = "itemized"

+++
## The Vote

When employees of the newly merged hospital belong to different unions the Ontario Labour Relations Board (OLRB) will hold a vote to see which union the employees want to have represent them with the new employer.  

Full time and part time service employees at Providence and St. Joseph’s belong to the Canadian Union Public Employees (CUPE) and full time and part time service employees at St. Mike’s belong to the Service Employees International Union (SEIU).  This means that there will be a vote between CUPE and SEIU to see which union will represent you in the workplace.  If a simple majority of those who vote, vote for CUPE, then CUPE will be the union for all service employees at all three hospital locations.

## Mergers:  the Composite Collective Agreement

During merger votes that are managed by the Public Sector Labour Relations Transition Act (PSLRTA) process, employees covered by a collective agreement continue to be covered by that collective agreement. 

> Rates of pay, benefits and working conditions continue to apply as they did prior to the vote, the employer can not make changes. That’s the law. 

When two or more unions represent employees before the representation vote, the collective agreements from these unions form a ‘composite’ collective agreement after the vote with one key difference - all employees, from all unions involved, will be covered by the successful union’s:

- seniority,  
- job posting,  
- layoff, recall & bumping language, and  
- grievance process

The ‘composite’ collective agreement, along with the successful union’s seniority and grievance provisions, remain in effect until a new collective agreement is negotiated between the employer and the successful union.  

Because CUPE bargains a Central Agreement with the OHA the new Collective Agreement will be comprised of CUPE’s Central Agreement and the ‘harmonized’ Local Issues Agreement which takes the best rates of pay and working conditions from all of the predecessor Collective Agreements so that no one loses as a result of the merger.  

Often employers see bargaining after the merger as a chance to gain concessions from the union.

CUPE wants to protect hard won gains that all unions have fought for over decades of bargaining.