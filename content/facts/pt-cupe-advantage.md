+++
author = "CUPE Hospitals"
date = "2019-01-01"
description = "Part time workers, benefits and the percent in lieu - The CUPE Advantage"
draft = false
format = "mergers"
tags = ["UHT Clerical", "Unity Health Toronto"]
title = "The CUPE Advantage for Part-time Workers"
featured = "/img/uploads/cupeadvantage.png"
featuredpath = "."
featuredalt = ""

+++

#### Sue

Sue is a part time CUPE member at St Joe’s and works **24 hours per week**.

Sue earns **$25.00 per hour** or $31,200 per year.

Because Sue is a CUPE member she also earns an additional **14 per cent
in lieu of benefits**, which is not reduced even though Sue participates
in the pension plan (HOOPP).

This now means that Sue earns an **additional $4,368 per year**.

Sue is now earning **$35,568** not including any per cent in lieu of vacation.

If Sue wants to buy benefits with the **$4,368** she can. If she has benefit coverage at another job or with a partner, she can use it on anything she likes.

For Sue, the CUPE advantage is **$4,368.**

#### Dave

Dave is a part time non-union employee at St Mike’s and **works 24 hours per week**.

Dave earns **$25.00 per hour** or $31 200 per year.


Because Dave is a non-union employee he gets a **credit of $1,100** that he can use to buy from the employer’s flex benefit plan.

Dave doesn’t get a per cent in lieu of benefits and if Dave doesn’t use the **$1,100 worth** of benefits he loses it because it can’t be cashed out or carried over to another year.

Dave earns **$31,200 per year** plus **$1,100 worth of benefits.**

For Dave, the CUPE advantage is **$4,368.**

#### Sandy

Sandy is a part time non-union employee at St Mike’s and usually works **12 hours per month**.

Sandy earns **$25.00 per hour**. Sandy earns **$3,600 per year**.

Because Sandy is a non-union employee and doesn’t qualify for the employer’s flex benefit plan she gets **8% in lieu of benefits**.

Before becoming a CUPE members, Sandy is earning **$3,888 per year**.

If Sandy was a CUPE member she would get **14% in lieu of benefits** and earn **$4,104 per year**.

For Sandy, the CUPE advantage is **$216**.

