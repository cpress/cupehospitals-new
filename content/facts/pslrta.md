+++
author = "CUPE Hospitals"
date = "2018-01-01"
description = "Public Sector Labour Relations Transition Act (PSLRTA)."
featured = "/img/pslrta-title.png"
format = "Mergers"
tags = ["Unity Health Toronto", "UHT Clerical"]
title = "CUPE's Detailed Guide to PSLRTA"
type = "itemized"

+++
When workplaces covered by PSLRTA amalgamate, merge or restructure, the Ontario Labour Relations Board (OLRB) will, in consultation with the affected unions and employers, determine what bargaining units are appropriate in the new workplace.

![](/img/pslrta-title.png)

## Table of Contents

1. [What is PSLRTA?](#what-is-pslrta)
2. [How does PSLRTA work?](#how-does-pslrta-work)
3. [What happens to my collective agreement](#what-happens-to-my-collective-agreement)
4. [Will my seniority still count?](#will-my-seniority-still-count)
5. [How does the vote work?](#how-does-the-vote-work)
6. [Sale of Business or Related Employer](#sale-of-business-or-related-employer)
7. [The campaign](#the-campaign)

## What is PSLRTA?

The *Public Sector Labour Relations Transition Act* (PSLRTA -- Pronounced Pah - SLUR - Ta) was passed in 1997 when Ontario began a major campaign of mergers, amalgamations, and restructurings in the municipal, school board, and hospital sectors. PSLRTA sets out a procedure for determining what bargaining unit is appropriate in the newly merged workplace and what union should represent the workers.

## How does PSLRTA work? 

When workplaces covered by PSLRTA amalgamate, merge or restructure, the Ontario Labour Relations Board ("Board") may, in consultation with the affected unions and employers, determine what bargaining unit structure is appropriate in the new workplace.

If the merger results in two or more unions representing workers in the post-merger bargaining unit, the board may order that a vote take place to determine which bargaining agent the employees wish to have represent them. All workers who would become members of the new bargaining unit are eligible to vote. For example, if the newly merged bargaining unit includes employees from two separate unions and employees who were not represented by a union prior to the merger - all employees in the new bargaining unit would be entitled to vote.

PSLRTA dictates that if one union represents 80% or more of the newly merged bargaining unit the Board can simply declare that union to be the bargaining agent without holding a vote.

> <h4>Example</h4>  
>  
> Two hospitals are merging. CUPE represents maintenance and custodial at one hospital, but they are represented by another union at the second hospital. The job categories covered by the two collective agreements are slightly different.  
>  
> The Board decides who will be included in a new bargaining unit that, after the merger, will include maintenance and custodial staff at both hospitals. Everyone who would be captured by the post-merger bargaining unit would be eligible to vote to be part of CUPE or the other union.  
>  
> After the vote, and until bargaining, all members of the new bargaining unit are covered by a “composite agreement,” and most of the terms of your CUPE agreement, including wages and benefits, will continue to be in effect for you, while former members of the other union will have terms included from their agreement that affect them, with a few exceptions. 

## What happens to my collective agreement? 

During the vote process, you are covered by your existing collective agreement.

After the vote, most of the provisions of your collective agreement continue to apply under a “composite agreement,” which will also include provisions from the other union’s collective agreement that affect their former members.

Under PSLRTA, anyone covered by a collective agreement continues to be covered by that collective agreement even after the representation vote. Rates of pay, hours of work, and other working conditions continue to apply to you as they did before the representation vote.

> Wages and benefits will not be reduced as a result of the representation vote. That’s the law.

However, the language from the winning union's collective agreement dealing seniority, grievance procedures, job postings, layoffs, and recall, will apply to everyone.

## Will my seniority still count?

Yes, seniority is “dovetailed.”

Unless the Board considers it inappropriate in the circumstances, the seniority of employees must be based solely on a common definition of seniority and determined with reference to the bargaining unit as a whole and not to only a part or parts of it. 

## How does the vote work?

There is a secret ballot vote conducted by the Board.  Your employer and no union may interfere in the process or attempt to threaten or intimidate voters.

The vote is typically done at the workplace during business hours. There are no advance polls, and there are no proxy votes.

The winning union must receive a majority of the votes case. If there multiple unions and a "no union" option on the ballot and no option achieves a majority of the votes case, the option with the least number of votes will be dropped and a run-off vote will be conducted. There will only be a "no union" option on the ballot if more than 40% of the appropriate bargaining unit were non-union prior to the merger. 

## Sale of Business or Related Employer 

For mergers, amalgamations, and restructuring that does not fall under the ambit of PSLRTA, CUPE can attempt to following our bargaining rights by filing a section 69 and/or section 1(4) application - otherwise known as a sale of business application and/or related employer application.

Section 69 protects a union’s bargaining rights should the employer sell its business. The new employer steps into the shoes of the old employer with respect to the collective agreement or any application for certification that has been filed. The Board has said that what happens is that a new collective agreement is effectively created with the successor employer.

Sale of business applications can be complicated by the presence of another union in the newly merged or acquired workplace. Board can determine what the appropriate bargaining units will be and order any representational votes necessary.

The Board has said that the vital consideration is whether the transferee has acquired from the transferor a functional economic vehicle, focusing on the ‘going concern’ or ‘dynamic activity’. The Board has stressed that the transfer of work alone is not sufficient to trigger the successorship provisions and preserve bargaining rights since the business is not synonymous with the employees or their work.

There is no conclusive test applied by the Board to determine if there has been a sale of business. When assessing whether a transfer of a business may be inferred, the Board has always been especially sensitive to any pre-existing corporate, commercial or familial relationship between the predecessor and the alleged successor, or between the predecessor, the alleged successor and a third party. Where there is a pre-existing corporate connection between the predecessor and the successor the Board has been disposed to infer a ‘transfer’ if there is the slightest evidence of such transaction.

With respect to related employer applications, the Board considers a number of factors to determine if two or more employers are under common direction and control including common ownership or financial control, common management, interrelationship of operations, representation to the public as a single, integrated enterprise, and centralized control of labour relations.

No single criteria is likely to decide the issue; however, the greater the degree of functional coherence and interdependence, the more probable it is that the entities will be treated as one employer.

The Board has found the following factors to be relevant: common premises, common equipment, common sales staff, interchange of employees, common facilities, common telephone, common supervision, common solicitors, common directors and officers, common labour relations and personnel policies, similar work performed by employees of the different entities, common sales techniques, functional integration and interdependence of operations, common cheques issued to employees common office, bookkeeping and accounting facilities and presence of signs indicating associated existence.

The common direction and control must be managerial in nature. The test under section 1(4) looks to uncover the ultimate source of power to direct employees.

## The campaign

If a vote is ordered by the Board, the unions involved will normally campaign in order to win the vote. The Board meets with the employer and the unions to determine the length of the campaign and the rules around accessing employees. This can include such things as access to home addresses, email lists, meetings in the workplace or space on bulletin boards.

CUPE is a great union with an excellent track record of protecting and making gains for our members. Even when the numbers look like they're stacked against us, we have proven we can win these types of representation votes. It takes every member getting involved and talking about what makes our union great.

Together we are stronger. Together, we will win. 








